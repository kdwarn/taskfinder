{
  description = "taskfinder - Display tasks from plain text files";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        cargoToml = pkgs.lib.importTOML ./Cargo.toml;
        manifest = cargoToml.package;
        binName = (builtins.head cargoToml.bin).name;
        cleanDescription =  pkgs.lib.removeSuffix "." manifest.description;

        package = with pkgs;
          rustPlatform.buildRustPackage rec {
            pname = manifest.name;
            version = manifest.version;

            src = fetchFromGitea {
              domain = "codeberg.org";
              owner = "kdwarn";
              repo = "taskfinder";
              rev = "v${version}";
              hash = "sha256-t5Lq7q/GKCZzdT3hpOI7TElKcAf5VMoGRMAcdf42eSo="; 
            };

            useFetchCargoVendor = true;
            cargoHash = "sha256-SulrnM2kkyvR3THPjKan4N69Qdha4VgM0hfIA+PFi8w="; 

            meta = with lib; {
              description = cleanDescription;
              homepage = manifest.repository;
              license = licenses.agpl3Plus;
              maintainers = with maintainers; [ Ginner ];
              platforms = platforms.all;
              mainProgram = binName;
            };
          };
      in
      {
        packages.default = package;

        apps.default = flake-utils.lib.mkApp {
          drv = package;
          name = binName;
        };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            cargo
            rustc
            rustfmt
            rust-analyzer
            clippy
          ];
        };
      }) // {
        overlay = final: prev: {
          taskfinder = self.packages.${prev.system}.default;
        };
      };
}
