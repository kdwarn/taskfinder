# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://codeberg.org/kdwarn/taskfinder/compare/v2.6.0...HEAD)

## [2.6.0](https://codeberg.org/kdwarn/taskfinder/compare/v2.5.1...v2.6.0) - 2025-03-04

### Added

* New "Tasks" mode! While the default mode, Files, lets you browse through files one by one and see all the tasks within it, Tasks mode collects all tasks from all files and lets you browse them in one place. Currently you are able to view incomplete tasks sorted by due date/priority or completed tasks sorted by completed date/priority. This mode is reached with `t`.
* Previously, tasks had no priority associated with them; priority was only at the level of files or tasks sets. But in Tasks mode it's useful to see the priority of a task and to sort tasks by priority (after due/completed date), and so now a task inherits the priority of the task set or file it belongs to. This only affects Tasks mode at present, but it opens up the possibility for more fine-grained priority sorting in Files mode in the future. 
* Now available as a Nix flake. Thanks [Ginner](https://codeberg.org/Ginner)!
* Ignore directories by placing their paths in a file named "ignore" in the configuration directory.

### Changed

* With Tasks mode now being reached by pressing `t`, this created a conflict with the key that had been used to filter by tags in Files mode. So, filtering by tags in Files mode now uses `/` (think "search" in many of your favorite Unix/Linux/GNU programs).

## [2.5.1](https://codeberg.org/kdwarn/taskfinder/compare/v2.5.0...v2.5.1) - 2025-01-14

Update the headers in this changelog.

## [2.5.0](https://codeberg.org/kdwarn/taskfinder/compare/v2.4.1...v2.5.0) - 2025-01-14

### Added

* You may now change the app's configuration from within the app (including resetting each setting to the default value), rather than editing the configuration file. This mode is reached with `x`.
* In Log mode, you can now toggle a line chart of your completed and incomplete tasks with `c`.
* Dialog boxes:
  * The cursor position is now shown.
  * You can now use the home and end keys to go to the beginning or end of the text.
  * In addition to deleting text with the backspace key, you can now also use the delete key.
* The info box in Log mode, in the set of changes between today's date and the selected date, now shows how many days ago that selected date was, to better contextualize the changes.
* In Help mode, you can view the app's version and changelog with the `v` key.

### Fixed

* The info box in Log mode will now show the difference between the first log record and the most recent one. Previously no changes (to previous entry or to most recent one) were shown.

### Changed

* Error handling and messaging has been greatly improved.

## [2.4.1](https://codeberg.org/kdwarn/taskfinder/compare/v2.4.0...v2.4.1) - 2024-11-02

### Fixed

* Updated and added Cargo.lock file.
* Fixed heading levels in this file.

## [2.4.0](https://codeberg.org/kdwarn/taskfinder/compare/v2.3.0...v2.4.0) - 2024-11-02

### Added

* Tasks that are in markdown lists, i.e. they start with a `- ` (with any amount of white space preceding them) will now be recognized.  
* Recognition of overdue dates. For incomplete tasks, if the due date is in the past, it will be colored red. Otherwise, dates (including dates in the past on completed tasks) will be green.

## [2.3.0](https://codeberg.org/kdwarn/taskfinder/compare/v2.2.1...v2.3.0) - 2024-10-18

### Added

* The info box (top right) in File mode now shows the number of completed *and* incomplete tasks for the current filters, for all files and the current file.

### Changed

* File mode:
  * If you only want to see incomplete tasks, and there are none for that file, a message will tell you that, rather than showing the task set heading and no tasks.
  * The display of tasks now always has consistent whitespace between task sets.
  * If all tasks in a task set are complete, but you're not including completed tasks in the view, then the header won't be displayed (previously just the tasks were excluded, and the header was still shown).
  * If all tasks in a task set are complete, and you're including completed tasks, the header will be dimmed, like all the tasks.
  * Task set headings are now colorized.
  * Filtering by due date and completion status are now decoupled. Previously, filtering by tasks with due dates would automatically set the filter for incomplete tasks as well.

### Fixed

* Files are no longer included if they contain no tasks. Previously, a file containing "TODO" (which marks the beginning of a task set) would be included even if there were no tasks in that file.
* Sort order of files in File mode. The priority of task sets that have no incomplete tasks will no longer be taken into account in ordering files.
* Line offset wasn't being reset between modes/tag filtering.

## [2.2.1](https://codeberg.org/kdwarn/taskfinder/compare/v2.2.0...v2.2.1) - 2024-08-23

### Fixed

* The default directory to search for files and tasks (~/taskfinder) was being created even if another directory was specified in the configuration file. This is no longer the case.

## [2.2.0](https://codeberg.org/kdwarn/taskfinder/compare/v2.1.1...v2.2.0) - 2024-08-07

### Added

* New "evergreen" mode! In the configuration file (e.g. ~/.config/taskfinder/config.toml on Linux), you can set a path to an "evergreen" file that you want to always have available, and is readily accessible simply by pressing `e`. I added this because I have a file that I use to keep track of tasks that I want to do each day, which I generally start fresh at the beginning of the day and update throughout. Perhaps you have a similar workflow or just want one particular file handy. Now it's possible! Or not - leave it unconfigured and the key for it won't show in the controls. (If you had a previous version of taskfinder installed and want to use this feature, you'll need to add a line to the config file like this: `evergreen_file = "path-to-file-here"`. If you are installing this version of taskfinder for the first time, you'll just need to add the path between the two double quotes, as it defaults to being empty.)

### Changed

* Rather than defaulting to use your entire home directory as the place to look for files and tasks, the program will create a new directory in your home directory named "taskfinder". This greatly speeds up startup and will also avoid any possible permission errors. However, the location to look for files and tasks remains configurable.

## [2.1.1](https://codeberg.org/kdwarn/taskfinder/compare/v2.1.0...v2.1.1) - 2024-08-03

### Fixed

* Navigation added to help mode.

## [2.1](https://codeberg.org/kdwarn/taskfinder/compare/v2.0.0...v2.1.0) - 2024-08-02

### Changed

* Modes! The different views of the program are now called modes, and the on-screen controls menu shows only the controls available for the mode you are in. "Files" is the starting/default mode, and can be reached from the other modes by pressing `f`. Press `l` to enter log mode, and `h` to enter help mode.

### Fixed

* Creation of the config and data directories previously assumed their parent directories (e.g. ~/.config and ~/.local/share, respectively) existed. If they didn't, the program would crash.
* Pressing Enter when there is no file to be edited no longer crashes the program.
* Filtering by due date will now only show *incomplete* tasks with due dates, as intended and as described in USAGE. 

## [2.0](https://codeberg.org/kdwarn/taskfinder/compare/v1.0.0...v2.0.0) - 2024-07-24

### Added

* The info box (top right) when viewing the log now includes changes between the selected row and the most recent row ("now"), enabling you to see changes on a larger scale.
* You can now mark when a task was completed by adding "completed:DATE" to a task, where DATE is in the format YYYY-MM-DD, e.g. "completed:2024-07-20". It will be colorized in magenta (at least for now).
* PageDown/PageUp: scroll 20 lines at a time.
* Projects/files can now be marked as archived.

### Changed

* Broke README into multiple files: README, INSTALL, USAGE. USAGE, rather than README, is now used for the in-app extended help text.
* If viewing the log, pressing ESC will reset selected row, rather than returning to the main view. To return to the main view, toggle the log off with `l`.
* Each individual task is now a `Task` struct rather than a simple string. This makes some things easy to code and should enable additional functionality, but otherwise doesn't change anything in use right now (aside from using completed dates, described above).
* Previously, completed tasks were listed after all incomplete tasks. Now they will be shown wherever they are placed in the taskset. If you use "subtasks" (e.g. further indent a task that is under another task), this is nice as they are kept with their parent task rather than placed at the bottom with all other completed tasks.

### Removed

* With the ability to archive files added, explicitly marking them as stale has been removed, since it's essentially the same functionality but "archived" seems more appropriate wording.

## [1.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.8.0...v1.0.0) - 2024-05-07

### Changed

The program was converted from a CLI to a TUI, so just about everything changed with how you use it, but in general it has the same functionality. Use is explained well, I think, between the "Controls" panel and the extended help section (which brings up the readme). In future releases I'll do more fine-grained descriptions of changes. 

## [0.8.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.7.0...v0.8.0) - 2024-03-25

### Added

* Priorities are now also recognized at the task-set level (in addition to being recognized at the file level).
* Example texts in README.

### Changed

* The table of tasks shown with `--log` now also shows the current number of tasks as the last row of the table.

### Fixed

* Completed tasks were not being shown when passing the `-c` flag if there were no incomplete tasks in a file.
* `--due` was including tasks without due dates, although it was limiting files correctly.

## [0.7.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.6.0...v0.7.0) - 2024-03-20

### Changed

* `--verbose` is now `--readme`, and the full README is included.
* `--history` is now `--log`, and the corresponding parameter in the configuration file has been changed from `task_history` to `num_tasks_log`. The path/filename associated with that parameter has also been changed, from "num_tasks_by_date" to "num_tasks_log". **If you had installed a previous version, the parameter name and value will need to be updated in the configuration file and the file will need to be renamed.**

## [0.6.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.5.0...v0.6.0) - 2024-03-18

### Added

* Show total number of incomplete or partially completed tasks.
* Show history of number of incomplete and complete tasks, overall and by priority

### Removed

* Previously, all output printed to the terminal in one shot, with per-file output behind the `--interactive` flag. Now, interactive mode is the default and only method for display.
* The `--short` flag. This showed only file paths and the first two lines of a file, and no tasks.
* Windows version/support.

## [0.5.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.4.0...v0.5.0) - 2024-02-24

### Added

* Due dates (with format "due:YYYY-MM-DD") are highlighted (in red) within the text of tasks.
* `--due` will only show files that have tasks that have due dates (and only those tasks that have a due date within those files).

## [0.4.0](https://codeberg.org/kdwarn/taskfinder/compare/v0.3.1...v0.4.0) - 2024-02-19

### Added

* Results are now sorted by priority level, from highest to lowest to none.

### Changed

* The "priority markers" (the text that represents a priority level) are now customizable, by altering the defaults that are written to the configuration file. 
* It is easier to specify a priority level to show results for. Use `-p1` to show tasks from files that have the highest priority, e.g.

## [0.3.1](https://codeberg.org/kdwarn/taskfinder/compare/v0.3.0...v0.3.1) - 2024-01-18

Readme extensively revised.

## [0.3.0](https://crates.io/crates/taskfinder/0.3.0) - 2024-01-14

Initial release.
