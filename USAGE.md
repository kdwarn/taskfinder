# Usage

This app is meant to be an aid in a task management system where tasks are located within plain text files. Tasks are identified and extracted from files by the following criteria:
  * the term "TODO" (in caps) must be in the text of the file
  * excluding whitespace, a line in a file starting with `[ ]` or `[]` identifies an incomplete task
  * excluding whitespace, a line in a file starting with `[X]` or `[x]` is a completed task
  * excluding whitespace, a line in a file starting with `[/]` is a partially completed task
  * this also works with markdown lists, so, excluding whitespace, lines starting with `- [ ]`, `- []`, `- [X]`, `- [x]`, `- [/]` are recognized as tasks.

A "task set" is a group of tasks. All tasks following a line containing "TODO" will be in a task set labelled as the full text of that line. Another line containing "TODO" within a file will end one task set and start another.

By default, files are defined as .md and .txt files in a folder named "taskfinder" in your home directory (it will be created if it doesn't exist). However, both the directory and the extensions are configurable. Upon first run, this program will create a configuration file at $HOME/.config/taskfinder/config.toml. You can either edit that file or use the configuration mode (`x`) to change the defaults. Also configurable are:
  * how many days constitutes a file being stale
  * whether to show completed tasks in Files mode by default
  * the text in a file that indicates a priority level ("priority markers")
  * the location of the evergreen file

## Modes

There are six possible modes, which are the top-level navigation of the app:
  * Files: View tasks per file. Press `f` to enter this mode. The app starts in this mode. Files are ordered by priority level and last modified date. There are a variety of filters that can be applied. Press `Enter` to open a file in your favorite terminal-based editor.
  * Tasks: View tasks from all files. Press `t` to enter this mode. Useful in viewing all incomplete tasks by due date or completed tasks by completed date.
  * Log: View log of task counts. Press `l`. 
  * Configuration: Adjust the app's settings. Press `x`.
  * Evergreen: View an "evergreen" file that you set in the configuration. Press `e`. This mode will not be shown as an option in the Controls panel unless a path to a file has been set.
  * Help: View help (this file). Press `h`. Within help mode, press `v` to view the version of the program you are using.

## Priorities

Priorities can be set at either the file level or the task-set level. To set the priority on a file, use one of the priority markers (see below) within the first two lines of a file. To set one at the task-set level, include one of the priority markers on the line that starts a task set, i.e. a line containing "TODO". Task sets without a priority will inherit the priority of the file, if any.

Priorities can be a string you define, but by default they are, from highest to lowest priority: `pri@1`, `pri@2`, `pri@3`, `pri@4`, `pri@5`. To change these, modify the `priority_markers` list in the configuration file. Note, however: **there must be five of them**.

Results will automatically be sorted by these priorities, with the tasks from files without one of these markers shown last.

In Files mode, toggle the priority filter by pressing 1-5.

## Tags

Unlike priorities, tags are defined at the file level only, and they must be in the first two lines of the file.

Tags are actually just whatever string you pass, so you may follow whatever convention you like. I use the form "@sometag".

In Files mode, press `/` to open a dialog box to enter a tag.

## Due and completed dates

If a task (see above on how a task is identified) contains a due date (in the form "due:YYYY-MM-DD"), the date will be colorized (red if in the past and the task is incomplete, green otherwise) in Files mode. Pressing `d` will show only those files that contain a due date.

You may also note when a task has been completed by adding "completed:YYYY-MM-DD" to its line. Like due dates, it will be colorized in Files mode.

In Tasks mode, incomplete tasks (the default view) are sorted by due date, while completed tasks are sorted by completed date. (Both have a secondary sort by priority.)

## Archived status

Projects can be completed (so I've heard), or perhaps temporarily or permanently abandoned. Archive them by including the text "@archived" anywhere within the project's file. Press `a` to toggle between archived and active files/tasks in both Files and Tasks modes.

## Staleness

If a file is considered stale (last modified more than `days_to_stale` days ago, with default of 365 initially set in the configuration), no tasks from it will be returned. Press `s` to toggle between stale and active files/tasks in both Files and Tasks mode.

## Completion Status

In Files mode, use `c` to toggle between only incomplete tasks and both incompleted/completed tasks (there is no way to view only completed tasks). This filter is slightly different than the others: changing it does not re-process and sort all of the files, but rather simply shows or hides completed tasks. This way, you always stay on the same file when you toggle it, therefore enabling a quick way to focus only on incomplete tasks, or to also view those tasks you've already completed.

In Tasks mode, you can view incomplete tasks (the default view) or completed tasks. Press `c` to toggle between the two.

## Log

The first time during a day that the program is run, it will log the number of complete and incomplete tasks - both overall and by priority. Note that completed tasks from stale or achived files are included in the count, but incomplete tasks from those files are not. `l` will enter Log mode, showing the log of tasks over time as a table. While in log mode, pressing `c` will display the log as a chart.

## Ignoring Directories

If you'd like to ignore certain directories so that they are excluded from discovery (so that no files or tasks are included from them), put them in a file named "ignore" in taskfinder's configuration folder. On Linux, that is ~/.config/taskfinder. Use absolute paths rather than relative paths, one per line. Be sure to spell out the path (e.g. use `/home/kris/<dir>` rather than `~/<dir>`.)

## Example files

Here is the text of two example files to demonstrate the conventions/expectations described above. Explanatory comments for our purposes here start with a '#'.

A file with no file-level priority:

```text
Tree planting                         # <- first line/title of the file
@thegreatoutdoors                     # <- tags; no priority at the file level.

TODO (preparation) (pri@1):           # <- a task set with level 1 (highest) priority.
  [x] identify sapling to transplant  # <- a completed task

TODO (actual planting) (pri@2):       # <- a task set with a level 2 priority.
  [/] dig a hole                      # <- a partially complete task
  [ ] dig up the sapling              # <- an incomplete task
  [] plant sapling                    # <- also an incomplete task
```

Another possible configuration:

```text
Tree planting                         
@thegreatoutdoors pri@2               # <- priority 2 at the file level

TODO (preparation):                   # <- a task set with no priority
  [x] identify sapling to transplant  
    - maybe the holly?                # <- a note, ignored by program

TODO:                                 # <- another task set with no priority
  [/] dig a hole                        
  [ ] dig up the sapling                
  [] plant sapling (due:2025-10-01)   # <- an incomplete task with a due date
```
