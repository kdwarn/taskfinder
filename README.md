# taskfinder

Keep your project-related tasks where they belong - with your notes on the project! This program, a terminal user interface (TUI), will extract and display them.

Installation: [INSTALL.md](https://codeberg.org/kdwarn/taskfinder/src/branch/main/INSTALL.md)

Changelog: [CHANGELOG.md](https://codeberg.org/kdwarn/taskfinder/src/branch/main/CHANGELOG.md)

Usage: [USAGE.md](https://codeberg.org/kdwarn/taskfinder/src/branch/main/USAGE.md)

License: [LICENSE.md](https://codeberg.org/kdwarn/taskfinder/src/branch/main/LICENSE.md)

Screenshots: https://kdwarn.net/projects/taskfinder

## Origins/Rationalization

For years, I used a task SaaS app to keep track of what I needed to do. However, I disliked this large amount of data about me and what I do being on someone else's server, and unencrypted to boot. So I started to just use pen and paper, organized by date. However, this doesn't allow for easy capturing of tasks that don't really have to be done on a particular date, or that belong to a single project. So I then started to write down tasks in my digital-file-based note-taking system (you can read more about that [here](https://kdwarn.net/blog/a-zettelkasten-with-vim-and-bash/)), where I also keep project notes. But then I had to start searching for them, and sometimes they were getting lost in the mix. So, `taskfinder` was born. 
