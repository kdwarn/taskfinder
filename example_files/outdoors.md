Yard and garden
@gardening @trees @wildlife

TODO (fall 2024):
  [x] identify sapling to transplant
  [x] dig a hole
  [x] dig up the sapling and plant it completed:2024-10-01
  [x] deep water once a week if no rain

TODO (winter 2024-25, pri@1):
  [x] read Tallamy's *Nature's Best Hope* completed:2024-12-23
  [ ] Read Kate Bradbury's *One Garden against the World* due:2025-01-31
  [ ] Read Chris Baines's *How to Make a Wildlife Garden* due:2025-02-28

TODO (spring 2025, pri@2):
  [ ] plant vegetable garden
  
TODO (summer 2025):
  [ ] build bat box
