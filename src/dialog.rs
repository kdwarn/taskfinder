#![forbid(unsafe_code)]
//! Editing text within a dialog box.

use ratatui::{
    prelude::Stylize,
    text::{Line, Span},
};

/// A dialog box that a user can enter/edit text in.
///
/// Based on <https://github.com/ratatui/ratatui/blob/v0.26.1/examples/user_input.rs>.
#[derive(PartialEq, Debug, Default)]
pub struct Dialog {
    pub up: bool,
    pub original: String,
    pub working_input: String,
    pub submitted_input: String,
    pub cursor_position: usize,
}

impl Dialog {
    pub fn move_cursor_left(&mut self) {
        let cursor_moved_left = self.cursor_position.saturating_sub(1);
        self.cursor_position = self.clamp_cursor(cursor_moved_left);
    }

    pub fn move_cursor_right(&mut self) {
        let cursor_moved_right = self.cursor_position.saturating_add(1);
        self.cursor_position = self.clamp_cursor(cursor_moved_right);
    }

    pub fn insert_char(&mut self, new_char: char) {
        self.working_input.insert(self.cursor_position, new_char);
        self.move_cursor_right();
    }

    pub fn backspace(&mut self) {
        if self.cursor_position != 0 {
            let current_index = self.cursor_position;
            let from_left_to_current_index = current_index - 1;

            // Get all characters before the selected character.
            let before_char_to_delete = self.working_input.chars().take(from_left_to_current_index);

            // Get all characters after selected character.
            let after_char_to_delete = self.working_input.chars().skip(current_index);

            // Put all characters together except the selected one, thus removing it.
            self.working_input = before_char_to_delete.chain(after_char_to_delete).collect();
            self.move_cursor_left();
        }
    }

    pub fn delete(&mut self) {
        let current_index = self.cursor_position;

        // Get all characters before the selected character.
        let before_char_to_delete = self.working_input.chars().take(current_index);

        // Get all characters after selected character.
        let after_char_to_delete = self.working_input.chars().skip(current_index + 1);

        // Put all characters together except the selected one, thus removing it.
        self.working_input = before_char_to_delete.chain(after_char_to_delete).collect();
    }

    pub fn end(&mut self) {
        self.cursor_position = self.working_input.chars().count();
    }

    pub fn home(&mut self) {
        self.cursor_position = 0;
    }

    pub fn clamp_cursor(&self, new_cursor_pos: usize) -> usize {
        new_cursor_pos.clamp(0, self.working_input.len())
    }

    pub fn reset(&mut self) {
        self.original.clear();
        self.working_input.clear();
        self.submitted_input.clear();
        self.cursor_position = 0;
    }

    /// Render working input of the dialog, showing cursor position.
    ///
    /// Note: This adds a space at the end (so the cursor can be shown at the end).
    /// This will likely need to be trimmed.
    pub fn render_working_input(&mut self) -> Line<'_> {
        // Get working input, adding a space after it.

        let text = format!("{} ", self.working_input);
        let text_len = text.chars().count();
        let text = text.chars();

        // Split the text up into before cursor, under cursor, and after cursor.
        let before_cursor = Span::raw(text.clone().take(self.cursor_position).collect::<String>());

        let under_cursor = Span::raw(
            text.clone()
                .skip(self.cursor_position)
                .take(1)
                .collect::<String>(),
        )
        .reversed();

        let after_cursor = if self.cursor_position != text_len {
            Span::raw(
                text.clone()
                    .skip(self.cursor_position + 1)
                    .collect::<String>(),
            )
        } else {
            Span::raw("")
        };

        Line::from(vec![before_cursor, under_cursor, after_cursor])
    }
}
