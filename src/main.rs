#![forbid(unsafe_code)]

use std::fs;
use std::io::{Stdout, stdout};
use std::path::Path;

use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen, disable_raw_mode, enable_raw_mode},
};
use ratatui::{
    Frame, Terminal,
    backend::CrosstermBackend,
    layout::{Constraint, Layout},
    prelude::Stylize,
    symbols::border,
    text::Line,
    widgets::{Borders, Paragraph, block::*},
};

use taskfinder::{
    App, FileStatus, FileWithTasks, RichTask, SIDEBAR_SIZE, TfError, USAGE,
    config::{Config, ConfigError},
    count::TaskCount,
    modes::{
        Mode,
        config_mode::{self, ConfigSetting},
        evergreen_mode, file_mode, help_mode,
        log_mode::{self, LogSubMode},
        task_mode,
    },
};

fn main() {
    let config = match Config::create_or_get() {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error configuring app: {e}");
            return;
        }
    };

    if config.priority_markers.len() != 5 {
        eprintln!(
            "{} Please fix before continuing.",
            ConfigError::IncorrectNumberOfPriorityMarkers
        );
        return;
    }

    // Get count of incomplete/partially complete Active tasks and all completed tasks.
    let current_active_count = match &FileWithTasks::collect(&config) {
        Ok(v) => TaskCount::extract(v, &FileStatus::Active),
        Err(e) => {
            eprintln!("Error getting current active task count from log: {e}");
            return;
        }
    };

    // Add that count to the data file.
    if let Err(e) = TaskCount::log(&config, current_active_count.clone()) {
        eprintln!("Error adding current task count to log: {e}");
        return;
    }

    // Get all log data, to store in app as default at start.
    let num_tasks_log = match fs::read_to_string(config.num_tasks_log.clone()) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error reading task log: {e}");
            return;
        }
    };

    let mut task_counts = num_tasks_log
        .lines()
        .map(TaskCount::new)
        .collect::<Vec<TaskCount>>();

    // Add the current active count to the log at top.
    task_counts.push(current_active_count);
    task_counts.reverse();

    // Initialize the app.
    let mut app = App::new(&config, task_counts);

    // Initialize the terminal.
    let mut terminal = match Terminal::new(CrosstermBackend::new(stdout())) {
        Ok(v) => v,
        Err(e) => {
            eprintln!("Error creating new terminal interface: {e}");
            return;
        }
    };

    if let Err(e) = execute!(terminal.backend_mut(), EnterAlternateScreen) {
        eprintln!("Errow switching to alternate screen: {e}");
        return;
    }

    if let Err(e) = enable_raw_mode() {
        eprintln!("Error entering raw mode: {e}");
        return;
    }

    // Run the app.
    if let Err(e) = run(&mut app, &mut terminal) {
        eprintln!("Error running app: {e}");
        return;
    }

    // Restore the terminal to its original state, then exit.
    if let Err(e) = disable_raw_mode() {
        eprintln!("Error disabling raw mode: {e}");
        return;
    }

    if let Err(e) = execute!(terminal.backend_mut(), LeaveAlternateScreen) {
        eprintln!("Error leaving alternate screen: {e}");
    }
}

/// Run the application's main loop until the user quits.
fn run(app: &mut App, terminal: &mut Terminal<CrosstermBackend<Stdout>>) -> Result<(), TfError> {
    // The app starts in File mode, so refine files to get files for default settings.
    file_mode::refine_files(app)?;

    while !app.exit {
        // Redraw the frame every time.
        terminal.draw(|frame| render(frame, app))?;

        // Watch for and handle user input.
        match event::read()? {
            // NOTE: it's important to check that the event is a key press event as
            // crossterm also emits key release and repeat events on Windows.
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                // Allow switching between modes, unless a dialog is open.
                if !app.filemode.tag_dialog.up && !app.configmode.dialog.up {
                    match key_event.code {
                        KeyCode::Char('q') => {
                            app.exit = true;
                            break;
                        }
                        KeyCode::Char('f') => {
                            file_mode::refine_files(app)?;
                            app.mode = Mode::File;
                            app.filemode.line_offset = 0;
                        }
                        KeyCode::Char('t') => {
                            app.taskmode.data = RichTask::collect(app)?;
                            app.mode = Mode::Task;
                            app.taskmode.table.select(Some(0));
                            app.filemode.line_offset = 0;
                        }
                        KeyCode::Char('l') => {
                            // Set back to table, the default submode.
                            app.logmode.submode = LogSubMode::Table;
                            // Recalculate latest log data - this is an expensive operation;
                            // only do it when log is opened, not during navigation of log.
                            if let Some(v) = app.logmode.data.first_mut() {
                                let active_count = TaskCount::extract(
                                    &FileWithTasks::collect(&app.config)?,
                                    &FileStatus::Active,
                                );
                                *v = active_count;
                            }
                            app.mode = Mode::Log;
                        }
                        KeyCode::Char('x') => {
                            app.mode = Mode::Config;
                            app.configmode.table.select(Some(0));
                            app.configmode.setting = ConfigSetting::get(0);
                            app.filemode.line_offset = 0;
                        }
                        KeyCode::Char('h') => {
                            app.mode = Mode::Help;
                            app.helpmode.help_text = USAGE.to_string();
                            app.helpmode.line_offset = 0;
                        }
                        KeyCode::Char('e') => {
                            if app.config.evergreen_file != Path::new("").to_path_buf() {
                                app.mode = Mode::Evergreen;
                                app.evergreenmode.line_offset = 0;
                            }
                        }
                        _ => (),
                    }
                }

                // Handle all other events, by mode.
                match app.mode {
                    Mode::File => file_mode::handle_input(app, terminal, key_event)?,
                    Mode::Task => task_mode::handle_input(app, terminal, key_event)?,
                    Mode::Log => log_mode::handle_input(app, key_event)?,
                    Mode::Help => help_mode::handle_input(app, key_event)?,
                    Mode::Evergreen => evergreen_mode::handle_input(app, terminal, key_event)?,
                    Mode::Config => config_mode::handle_input(app, key_event)?,
                }
            }
            _ => (),
        };
    }
    Ok(())
}

fn render(frame: &mut Frame, app: &mut App) {
    // Layout the rectangles of the UI.
    let horizontal = Layout::horizontal([Constraint::Fill(1), Constraint::Length(SIDEBAR_SIZE)]);
    let vertical = Layout::vertical([Constraint::Percentage(41), Constraint::Percentage(59)]);

    let [left, right] = horizontal.areas(frame.area());
    let [top_right, bottom_right] = vertical.areas(right);

    // Main content.
    // The surrounding block of the main content.
    let main_block = Block::default()
        .title_top(Line::from(" Taskfinder ").bold().centered())
        .borders(Borders::ALL)
        .border_set(border::THICK)
        .padding(Padding::horizontal(1));

    // Top right - additional info related to main content.
    let info_block = Block::default()
        .borders(Borders::ALL)
        .border_set(border::THICK);

    // Bottom right - Controls menu
    let controls_block = Block::default()
        .title_top(Line::from(" Controls").centered().bold())
        .title_bottom(Line::from("q to quit").centered())
        .borders(Borders::ALL)
        .border_set(border::THICK);

    // Controls content common to all modes.
    let mut controls_content: Vec<Line<'_>> = vec![
        Line::from("Mode".blue().bold().underlined()),
        vec!["f".blue(), " Files".into(), "   x".blue(), " Config".into()].into(),
        vec!["t".blue(), " Tasks".into(), "   h".blue(), " Help".into()].into(),
    ];

    // Only show evergreen mode if a file has been set in the configuration.
    if app.config.evergreen_file != Path::new("").to_path_buf() {
        controls_content.push(
            vec![
                "l".blue(),
                " Log".into(),
                "     e".blue(),
                " Evergreen ".into(),
            ]
            .into(),
        );
    } else {
        controls_content.push(vec!["l".blue(), " Log".into()].into());
    }

    match app.mode {
        Mode::File => file_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
            vertical,
        ),
        Mode::Task => task_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
        ),
        Mode::Config => config_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
        ),
        Mode::Log => log_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
            vertical,
        ),
        Mode::Help => help_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
        ),
        Mode::Evergreen => evergreen_mode::render(
            app,
            frame,
            info_block,
            main_block,
            top_right,
            left,
            &mut controls_content,
        ),
    }

    let controls = Paragraph::new(controls_content).block(controls_block);
    frame.render_widget(controls, bottom_right);
}
