#![forbid(unsafe_code)]

use std::cmp::Reverse;
use std::fmt::{self, Display, Formatter};
use std::io;
use std::path::{Path, PathBuf};
use std::process::{Command, ExitStatus};
use std::str::FromStr;
use std::sync::LazyLock;
use std::time::SystemTime;
use std::{env, fs};

use chrono::NaiveDate;
use ratatui::layout::{Constraint, Layout, Rect};
use ratatui::widgets::TableState;
use regex::Regex;
use thiserror::Error;

pub mod config;
pub mod count;
pub mod dialog;
pub mod modes;

use config::{Config, ConfigError};
use count::TaskCount;
use dialog::Dialog;
use modes::{
    Mode,
    config_mode::ConfigMode,
    evergreen_mode::EvergreenMode,
    file_mode::FileMode,
    help_mode::HelpMode,
    log_mode::{LogMode, LogSubMode},
    task_mode::TaskMode,
};

pub const USAGE: &str = include_str!("../USAGE.md");
pub const SIDEBAR_SIZE: u16 = 38;

// Configure regex for finding due dates.
pub static DUE_DATE_RE: LazyLock<Regex> =
    LazyLock::new(|| Regex::new(r"(?<due_date>due:[0-9]{4}-[0-9]{2}-[0-9]{2})").unwrap());
pub static COMPLETED_DATE_RE: LazyLock<Regex> = LazyLock::new(|| {
    Regex::new(r"(?<completed_date>completed:[0-9]{4}-[0-9]{2}-[0-9]{2})").unwrap()
});

/// The errors that can occur.
#[derive(Error, Debug)]
pub enum TfError {
    #[error("IoError: {0}")]
    Io(#[from] io::Error),
    #[error("System time error")]
    SystemTime(#[from] std::time::SystemTimeError),
    #[error("Parsing error")]
    ParseInt(#[from] std::num::ParseIntError),
    #[error("No matching priority.")]
    NoMatchingPriority,
    #[error("Path does not exist.")]
    NoSuchPath,
    #[error("{0}")]
    ConfigError(#[from] config::ConfigError),
}

/// The data and state of the app.
pub struct App {
    pub mode: Mode,
    pub config: Config,
    pub filemode: FileMode,
    pub logmode: LogMode,
    pub configmode: ConfigMode,
    pub helpmode: HelpMode,
    pub evergreenmode: EvergreenMode,
    pub taskmode: TaskMode,
    pub exit: bool,
}

impl App {
    pub fn new(config: &Config, task_counts: Vec<TaskCount>) -> Self {
        Self {
            mode: Mode::File,
            config: config.clone(),
            filemode: FileMode {
                current_file: 0,
                files: vec![],
                completed: config.include_completed,
                due: false,
                file_status: FileStatus::Active,
                priority: None,
                tag_dialog: Dialog::default(),
                line_offset: 0,
            },
            logmode: LogMode {
                submode: LogSubMode::Table,
                data: task_counts,
                table: TableState::default().with_selected(0),
            },
            configmode: ConfigMode {
                table: TableState::default().with_selected(0),
                setting: None,
                dialog: Dialog::default(),
                message_to_user: None,
            },
            helpmode: HelpMode {
                line_offset: 0,
                help_text: USAGE.to_string(),
            },
            evergreenmode: EvergreenMode { line_offset: 0 },
            taskmode: TaskMode {
                data: vec![],
                table: TableState::default().with_selected(0),
                file_status: FileStatus::Active,
                completion_status: CompletionStatus::Incomplete,
            },
            exit: false,
        }
    }
}

/// Priority of a file, task set, or task, from lowest to highest.
#[derive(Clone, Copy, PartialEq, PartialOrd, Ord, Hash, Eq, Debug)]
pub enum Priority {
    NoPriority,
    Five,
    Four,
    Three,
    Two,
    One,
}

impl Priority {
    /// Extract the highest priority from some amount text.
    pub fn extract(haystack: &str) -> Priority {
        let priorities = vec![
            Priority::One,
            Priority::Two,
            Priority::Three,
            Priority::Four,
            Priority::Five,
        ];
        let mut priority = Priority::NoPriority;

        for each in priorities {
            if haystack.contains(&format!("{each}")) {
                priority = each;
                break; // return early, to return highest priority found
            }
        }
        priority
    }
}

impl Display for Priority {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let priority_markers = Config::priority_markers();
        let priority_str = match self {
            Priority::One => priority_markers[0].to_string(),
            Priority::Two => priority_markers[1].to_string(),
            Priority::Three => priority_markers[2].to_string(),
            Priority::Four => priority_markers[3].to_string(),
            Priority::Five => priority_markers[4].to_string(),
            Priority::NoPriority => "".to_string(),
        };
        write!(f, "{}", priority_str)
    }
}

impl FromStr for Priority {
    type Err = TfError;
    fn from_str(s: &str) -> Result<Priority, TfError> {
        let priority_markers = Config::priority_markers();
        match s {
            s if s == priority_markers[0] => Ok(Priority::One),
            s if s == priority_markers[1] => Ok(Priority::Two),
            s if s == priority_markers[2] => Ok(Priority::Three),
            s if s == priority_markers[3] => Ok(Priority::Four),
            s if s == priority_markers[4] => Ok(Priority::Five),
            _ => Err(TfError::NoMatchingPriority),
        }
    }
}

/// How a file can be categorized.
#[derive(PartialEq, Clone, Debug)]
pub enum FileStatus {
    Active,
    Archived,
    Stale,
}

/// Status of a task.
pub enum CompletionStatus {
    Incomplete,
    Completed,
}

/// A file that contains tasks.
#[derive(Debug, Clone)]
pub struct FileWithTasks {
    pub file: PathBuf,
    pub last_modified: SystemTime,
    pub priority: Priority,
    pub status: FileStatus,
    pub has_due_date: bool,
    pub head: Vec<String>,
    pub task_sets: Vec<TaskSet>,
}

impl FileWithTasks {
    /// Get all files from configured directory that contain tasks.
    pub fn collect(config: &Config) -> Result<Vec<FileWithTasks>, TfError> {
        // Gather all possible files to examine.
        let paths = collect_file_paths(&config.path, &config.file_extensions, vec![])?;

        // Extract the tasks.
        let mut files_with_tasks: Vec<FileWithTasks> = vec![];
        for path in &paths {
            if let Ok(Some(v)) = Self::extract(path, config.days_to_stale) {
                files_with_tasks.push(v);
            }
        }
        // Sort tasks sets within files.
        for file in files_with_tasks.iter_mut() {
            file.task_sets
                .sort_unstable_by_key(|task_set| Reverse(task_set.priority))
        }

        Ok(files_with_tasks)
    }

    /// Extract data from a file.
    pub fn extract(path: &Path, days_to_stale: u64) -> Result<Option<FileWithTasks>, TfError> {
        let contents = match fs::read_to_string(path) {
            Ok(v) => v,
            Err(_) => return Ok(None),
        };

        if contents.contains("TODO") {
            let last_modified = fs::metadata(path)?.modified()?;

            // Set file status.
            let stale_threshold = std::time::Duration::from_secs(60 * 60 * 24 * days_to_stale);
            let status = if contents.contains("@archived") {
                FileStatus::Archived
            } else if last_modified.elapsed()? > stale_threshold {
                FileStatus::Stale
            } else {
                FileStatus::Active
            };

            // The first two lines of the file - the title and tags - are the head.
            let head: Vec<String> = contents.lines().take(2).map(String::from).collect();

            // file-level priority
            let file_priority = Priority::extract(&head.join("\n"));

            // Create the FileWithTasks.
            let mut file_with_tasks = FileWithTasks {
                file: path.to_path_buf(),
                last_modified,
                priority: file_priority,
                status,
                has_due_date: false,
                head,
                task_sets: vec![],
            };

            let mut task_set: Option<TaskSet> = None;
            for line in contents.lines() {
                // Don't do anything with a line until there's a taskset.
                if task_set.is_none() && !line.trim().contains("TODO") {
                    continue;
                }

                // Create TaskSet if we come to a TODO section
                if line.trim().contains("TODO") {
                    // If there was an existing TaskSet, we've come to a new one,
                    // so save the existing one by appending it to Vec we're returning.
                    if let Some(v) = task_set {
                        file_with_tasks.task_sets.push(v);
                    }
                    // Start new TaskSet.
                    let mut task_set_priority = Priority::extract(line);
                    // If the task_set has no priority, it inherits the priority of its file, if any.
                    if matches!(task_set_priority, Priority::NoPriority)
                        && !matches!(file_priority, Priority::NoPriority)
                    {
                        task_set_priority = file_priority;
                    }
                    task_set = Some(TaskSet::new(line.to_string(), task_set_priority));
                }

                if line.trim().starts_with("[ ]")                // incomplete
            || line.trim().starts_with("- [ ]")              // incomplete
            || line.trim().starts_with("[/]")                // partially complete
            || line.trim().starts_with("- [/]")              // partially complete
            || line.trim().starts_with("[\\]")               // partially complete
            || line.trim().starts_with("- [\\]")             // partially complete
            || line.trim().starts_with("[]")                 // incomplete
            || line.trim().starts_with("- []")               // incomplete
            || line.trim().to_lowercase().starts_with("[x]") // completed
            || line.trim().to_lowercase().starts_with("- [x]")
                {
                    let mut task = if line.trim().to_lowercase().starts_with("[x]")
                        || line.trim().to_lowercase().starts_with("- [x]")
                    {
                        Task {
                            text: line.to_string(),
                            completed: true,
                            completed_date: None,
                            due_date: None,
                            priority: Priority::NoPriority,
                        }
                    } else {
                        Task {
                            text: line.to_string(),
                            completed: false,
                            due_date: None,
                            completed_date: None,
                            priority: Priority::NoPriority,
                        }
                    };

                    // Set priority at the task level, inheriting if necessary.
                    // The check at the beginning of this loop should ensure that we're in a task set,
                    // but check it anyway.
                    if let Some(ref v) = task_set {
                        let task_priority = Priority::extract(&task.text);

                        if task_priority == Priority::NoPriority {
                            if v.priority == Priority::NoPriority {
                                if file_priority == Priority::NoPriority {
                                    task.priority = Priority::NoPriority;
                                } else {
                                    task.priority = file_priority;
                                }
                            } else {
                                task.priority = v.priority;
                            }
                        } else {
                            task.priority = task_priority;
                        }
                    }

                    if let Some(due_date) = DUE_DATE_RE.captures(&task.text) {
                        // Strip the "due:date" part from the string and put the result as the task.
                        let due_date = due_date["due_date"].to_string();
                        task.text = task.text.replace(&due_date, "");
                        task.text = task.text.trim_end().to_string();
                        // Store due date, after converting from string.
                        task.due_date = Some(
                            NaiveDate::parse_from_str(
                                due_date.strip_prefix("due:").unwrap(),
                                "%Y-%m-%d",
                            )
                            .unwrap(),
                        );
                    }

                    if let Some(completed_date) = COMPLETED_DATE_RE.captures(&task.text) {
                        let completed_date = completed_date["completed_date"].to_string();
                        task.text = task.text.replace(&completed_date, "");
                        task.text = task.text.trim_end().to_string();
                        // Store completed date, after converting from string.
                        task.completed_date = Some(
                            NaiveDate::parse_from_str(
                                completed_date.strip_prefix("completed:").unwrap(),
                                "%Y-%m-%d",
                            )
                            .unwrap(),
                        );
                    };

                    if let Some(ref mut v) = task_set {
                        if task.due_date.is_some() {
                            file_with_tasks.has_due_date = true;
                        }
                        v.tasks.push(task);
                    }
                }
            }
            // Add last/only TaskSet to the FileWithTasks
            if let Some(v) = task_set {
                file_with_tasks.task_sets.push(v);
            }

            // If any task set contains tasks, return it. Otherwise (if all tasks sets are empty),
            // it falls through to returning None.
            for task_set in &file_with_tasks.task_sets {
                if !task_set.tasks.is_empty() {
                    return Ok(Some(file_with_tasks));
                }
            }
            Ok(None)
        } else {
            Ok(None)
        }
    }
}

/// A group of tasks.
#[derive(Debug, Clone)]
pub struct TaskSet {
    pub section: String,
    pub priority: Priority,
    pub tasks: Vec<Task>,
}

impl TaskSet {
    fn new(section: String, priority: Priority) -> Self {
        Self {
            section,
            priority,
            tasks: vec![],
        }
    }
}

/// An individual task.
#[derive(Debug, Clone)]
pub struct Task {
    pub text: String,
    pub completed: bool,
    pub due_date: Option<NaiveDate>,
    pub completed_date: Option<NaiveDate>,
    pub priority: Priority,
}

/// A task that includes the task set and file it belongs to.
#[derive(Debug, Clone)]
pub struct RichTask {
    pub task: Task,
    pub task_set: String,
    pub file_name: String,
    pub file_path: PathBuf,
    pub file_status: FileStatus,
}

impl RichTask {
    /// Collect all rich tasks.
    pub fn collect(app: &App) -> Result<Vec<Self>, TfError> {
        // Get all tasks.
        let files = FileWithTasks::collect(&app.config)?;
        let mut tasks = vec![];
        for file in files {
            for task_set in file.task_sets {
                for task in task_set.tasks {
                    tasks.push(RichTask {
                        task,
                        task_set: task_set.section.clone(),
                        file_name: file.head[0].clone(),
                        file_path: file.file.clone(),
                        file_status: file.status.clone(),
                    })
                }
            }
        }

        // Filter.
        match app.taskmode.file_status {
            FileStatus::Active => {
                tasks.retain(|task| task.file_status == FileStatus::Active);
            }
            FileStatus::Archived => {
                tasks.retain(|task| task.file_status == FileStatus::Archived);
            }
            FileStatus::Stale => {
                tasks.retain(|task| task.file_status == FileStatus::Stale);
            }
        }

        match app.taskmode.completion_status {
            CompletionStatus::Incomplete => {
                tasks.retain(|task| !task.task.completed);
            }
            CompletionStatus::Completed => {
                tasks.retain(|task| task.task.completed);
            }
        }

        // Sort, first by priority.
        tasks.sort_by_key(|task| Reverse(task.task.priority));

        // Sort, next by due or completed due.
        match app.taskmode.completion_status {
            CompletionStatus::Incomplete => {
                // Split vec in two in order to do this - otherwise it will put those without
                // due dates first.
                let mut with_due_dates = tasks.clone();
                with_due_dates.retain(|task| task.task.due_date.is_some());
                with_due_dates.sort_by_key(|task| task.task.due_date);

                let mut without_due_dates = tasks;
                without_due_dates.retain(|task| task.task.due_date.is_none());

                with_due_dates.append(&mut without_due_dates);
                tasks = with_due_dates;
            }
            CompletionStatus::Completed => {
                tasks.sort_by_key(|task| Reverse(task.task.completed_date));
            }
        }

        Ok(tasks)
    }
}

/// Collect all paths within configured directory and with specified extensions, ignoring any
/// full paths listed in the ignore file.
fn collect_file_paths(
    dir: &Path,
    extensions: &Vec<String>,
    mut results: Vec<PathBuf>,
) -> Result<Vec<PathBuf>, TfError> {
    // Get ignored directories.
    let mut config_dir = match dirs::config_dir() {
        Some(v) => v,
        None => return Err(ConfigError::LocateConfigDir)?,
    };
    config_dir.push("taskfinder/ignore");
    let ignored_dirs = match fs::read_to_string(config_dir) {
        Ok(v) => {
            let dirs = v
                .lines()
                .filter(|l| !l.is_empty())
                .map(Path::new)
                .map(|p| p.to_path_buf())
                .collect::<Vec<_>>();
            dirs
        }
        Err(_) => vec![],
    };

    // Loop through directories, excluding ignored, and
    if dir.is_dir() {
        'outer: for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                if !ignored_dirs.is_empty() {
                    for ignored_dir in &ignored_dirs {
                        if path.starts_with(ignored_dir) {
                            continue 'outer;
                        }
                    }
                }
                results = collect_file_paths(&path, extensions, results)?;
            } else if let Some(v) = path.extension() {
                if extensions.contains(&v.to_str().unwrap().to_owned()) {
                    results.push(path)
                }
            }
        }
    }
    Ok(results)
}

/// Open a file for editing.
pub fn edit(file: PathBuf) -> io::Result<ExitStatus> {
    let editor = match env::var("EDITOR") {
        Ok(v) => v,
        Err(_) => String::from("vim"),
    };
    Command::new(editor).args([file]).status()
}

/// Create a centered rect, offset by sidebar.
///
/// Modified version of <https://ratatui.rs/how-to/layout/center-a-rect/>.
pub fn centered_rect(x: u16, y: u16, r: Rect, sidebar_size: u16) -> Rect {
    let dialog_layout = Layout::vertical([
        Constraint::Fill(1),
        Constraint::Length(y),
        Constraint::Fill(1),
    ])
    .split(r);

    Layout::horizontal([
        Constraint::Fill(1),
        Constraint::Length(x),
        Constraint::Fill(1),
        Constraint::Length(sidebar_size),
    ])
    .split(dialog_layout[1])[1]
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn extract_correct_number_of_task_sets() {
        let file = Path::new("example_files/outdoors.md");
        let file_with_tasks = FileWithTasks::extract(file, 365).unwrap().unwrap();
        assert_eq!(file_with_tasks.task_sets.len(), 4);

        let file = Path::new("example_files/no_priority.md");
        let file_with_tasks = FileWithTasks::extract(file, 365).unwrap().unwrap();
        assert_eq!(file_with_tasks.task_sets.len(), 1)
    }

    #[test]
    fn extract_correct_number_of_tasks_in_task_set() {
        let file = Path::new("example_files/outdoors.md");
        let task_sets = FileWithTasks::extract(file, 365)
            .unwrap()
            .unwrap()
            .task_sets
            .clone();
        assert_eq!(task_sets[0].tasks.len(), 4);
        assert_eq!(task_sets[1].tasks.len(), 3);
        assert_eq!(task_sets[2].tasks.len(), 1);
        assert_eq!(task_sets[3].tasks.len(), 1);

        let file = Path::new("example_files/no_priority.md");
        let task_sets = FileWithTasks::extract(file, 365)
            .unwrap()
            .unwrap()
            .task_sets
            .clone();
        assert_eq!(task_sets[0].tasks.len(), 2);
    }

    #[test]
    fn tasks_ignored_with_no_todo_header() {
        let file = Path::new("example_files/no_tasks.md");
        let task_sets = FileWithTasks::extract(file, 365).unwrap();
        assert!(task_sets.is_none());
    }

    #[test]
    fn highest_priority_in_file_is_correct() {
        let priority =
            Priority::extract(&fs::read_to_string(Path::new("example_files/outdoors.md")).unwrap());
        assert_eq!(priority, Priority::One);

        let priority = Priority::extract(
            &fs::read_to_string(Path::new("example_files/no_priority.md")).unwrap(),
        );
        assert_eq!(priority, Priority::NoPriority);
    }
}
