//! Counting tasks, including logging them to file.
#![forbid(unsafe_code)]

use std::fs::{self, File};
use std::io::Write;

use chrono::{Local, NaiveDate};
use ratatui::{layout::Alignment, text::Text};

use crate::{FileStatus, FileWithTasks, Priority, TfError, config::Config};

#[derive(Clone, Debug)]
pub struct TaskCount {
    pub date: NaiveDate,
    pub incomplete: usize,
    pub complete: usize,
    pub p1_incomp: usize,
    pub p1_comp: usize,
    pub p2_incomp: usize,
    pub p2_comp: usize,
    pub p3_incomp: usize,
    pub p3_comp: usize,
    pub p4_incomp: usize,
    pub p4_comp: usize,
    pub p5_incomp: usize,
    pub p5_comp: usize,
}

impl TaskCount {
    pub fn new(line: &str) -> Self {
        let elements: Vec<String> = line.split(',').map(str::to_string).collect();
        Self {
            date: NaiveDate::parse_from_str(&elements[0], "%Y-%m-%d").unwrap(),
            incomplete: elements[1].parse().unwrap(),
            complete: elements[2].parse().unwrap(),
            p1_incomp: elements[3].parse().unwrap(),
            p1_comp: elements[4].parse().unwrap(),
            p2_incomp: elements[5].parse().unwrap(),
            p2_comp: elements[6].parse().unwrap(),
            p3_incomp: elements[7].parse().unwrap(),
            p3_comp: elements[8].parse().unwrap(),
            p4_incomp: elements[9].parse().unwrap(),
            p4_comp: elements[10].parse().unwrap(),
            p5_incomp: elements[11].parse().unwrap(),
            p5_comp: elements[12].parse().unwrap(),
        }
    }

    pub fn as_strings(&self) -> Vec<String> {
        vec![
            self.date.to_string(),
            self.incomplete.to_string(),
            self.complete.to_string(),
            self.p1_incomp.to_string(),
            self.p1_comp.to_string(),
            self.p2_incomp.to_string(),
            self.p2_comp.to_string(),
            self.p3_incomp.to_string(),
            self.p3_comp.to_string(),
            self.p4_incomp.to_string(),
            self.p4_comp.to_string(),
            self.p5_incomp.to_string(),
            self.p5_comp.to_string(),
        ]
    }
    pub fn labels() -> Vec<Text<'static>> {
        vec![
            Text::from("date").alignment(Alignment::Left),
            Text::from("incomplete").alignment(Alignment::Right),
            Text::from("complete").alignment(Alignment::Right),
            Text::from("p1[ ]").alignment(Alignment::Right),
            Text::from("p1[x]").alignment(Alignment::Right),
            Text::from("p2[ ]").alignment(Alignment::Right),
            Text::from("p2[x]").alignment(Alignment::Right),
            Text::from("p3[ ]").alignment(Alignment::Right),
            Text::from("p3[x]").alignment(Alignment::Right),
            Text::from("p4[ ]").alignment(Alignment::Right),
            Text::from("p4[x]").alignment(Alignment::Right),
            Text::from("p5[ ]").alignment(Alignment::Right),
            Text::from("p5[x]").alignment(Alignment::Right),
        ]
    }

    /// Extract a count from a set of files by their status.
    pub fn extract(files: &[FileWithTasks], file_status: &FileStatus) -> TaskCount {
        let date = Local::now().date_naive();
        let mut task_count = TaskCount {
            date,
            incomplete: 0,
            complete: 0,
            p1_incomp: 0,
            p2_incomp: 0,
            p3_incomp: 0,
            p4_incomp: 0,
            p5_incomp: 0,
            p1_comp: 0,
            p2_comp: 0,
            p3_comp: 0,
            p4_comp: 0,
            p5_comp: 0,
        };
        for file_with_task in files {
            for task_set in &file_with_task.task_sets {
                let count_completed = task_set.tasks.iter().filter(|task| task.completed).count();

                // Don't count incomplete stale or incomplete archived tasks when counting tasks
                // from active files.
                let count_incomplete = match file_status {
                    FileStatus::Active => {
                        if matches!(file_with_task.status, FileStatus::Active) {
                            task_set.tasks.iter().filter(|task| !task.completed).count()
                        } else {
                            0
                        }
                    }
                    _ => task_set.tasks.iter().filter(|task| !task.completed).count(),
                };

                task_count.incomplete += count_incomplete;
                task_count.complete += count_completed;
                if (file_with_task.priority == Priority::One
                    && task_set.priority == Priority::NoPriority)
                    || task_set.priority == Priority::One
                {
                    task_count.p1_incomp += count_incomplete;
                    task_count.p1_comp += count_completed;
                }
                if (file_with_task.priority == Priority::Two
                    && task_set.priority == Priority::NoPriority)
                    || task_set.priority == Priority::Two
                {
                    task_count.p2_incomp += count_incomplete;
                    task_count.p2_comp += count_completed;
                }
                if (file_with_task.priority == Priority::Three
                    && task_set.priority == Priority::NoPriority)
                    || task_set.priority == Priority::Three
                {
                    task_count.p3_incomp += count_incomplete;
                    task_count.p3_comp += count_completed;
                }
                if (file_with_task.priority == Priority::Four
                    && task_set.priority == Priority::NoPriority)
                    || task_set.priority == Priority::Four
                {
                    task_count.p4_incomp += count_incomplete;
                    task_count.p4_comp += count_completed;
                }
                if (file_with_task.priority == Priority::Five
                    && task_set.priority == Priority::NoPriority)
                    || task_set.priority == Priority::Five
                {
                    task_count.p5_incomp += count_incomplete;
                    task_count.p5_comp += count_completed;
                }
            }
        }
        task_count
    }

    /// Log the number of tasks for the current date to file.
    ///
    /// A line will only be entered if that date is not yet in the file.
    pub fn log(config: &Config, task_count: TaskCount) -> Result<(), TfError> {
        let date = Local::now().date_naive();
        let contents = fs::read_to_string(&config.num_tasks_log)?;
        if !contents.contains(&format!("{date}")) {
            let mut f = File::options()
                .append(true)
                .open(config.num_tasks_log.clone())?;

            writeln!(
                &mut f,
                "{},{},{},{},{},{},{},{},{},{},{},{},{}",
                task_count.date,
                task_count.incomplete,
                task_count.complete,
                task_count.p1_incomp,
                task_count.p1_comp,
                task_count.p2_incomp,
                task_count.p2_comp,
                task_count.p3_incomp,
                task_count.p3_comp,
                task_count.p4_incomp,
                task_count.p4_comp,
                task_count.p5_incomp,
                task_count.p5_comp,
            )?;
        }

        Ok(())
    }
}
