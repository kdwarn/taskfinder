#![forbid(unsafe_code)]
//! View the log, as table or chart.

use std::io;

use chrono::{Days, Local, NaiveDate};
use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    Frame,
    layout::{Alignment, Constraint, Layout, Margin, Rect},
    style::{Color, Modifier, Style, Stylize},
    symbols,
    text::{Line, Text},
    widgets::{
        Axis, Block, Chart, Dataset, GraphType, LegendPosition, Paragraph, Row, Table, TableState,
        WidgetRef, Wrap,
    },
};

use crate::{App, count::TaskCount};

#[derive(Default)]
pub enum LogSubMode {
    #[default]
    Table,
    Chart,
}

#[derive(Default)]
pub struct LogMode {
    pub submode: LogSubMode,
    pub data: Vec<TaskCount>,
    pub table: TableState,
}

/// Handle user input in log mode.
pub fn handle_input(app: &mut App, key_event: KeyEvent) -> io::Result<()> {
    match app.logmode.submode {
        LogSubMode::Table => match key_event.code {
            KeyCode::Char('j') | KeyCode::Down => {
                let i = match app.logmode.table.selected() {
                    Some(i) => {
                        if i >= app.logmode.data.len() - 1 {
                            0
                        } else {
                            i + 1
                        }
                    }
                    None => 0,
                };
                app.logmode.table.select(Some(i));
            }
            KeyCode::Char('k') | KeyCode::Up => {
                let i = match app.logmode.table.selected() {
                    Some(i) => {
                        if i == 0 {
                            app.logmode.data.len() - 1
                        } else {
                            i - 1
                        }
                    }
                    None => 0,
                };
                app.logmode.table.select(Some(i));
            }
            KeyCode::Esc => app.logmode.table = TableState::default().with_selected(0),
            KeyCode::Char('c') => app.logmode.submode = LogSubMode::Chart,
            _ => (),
        },
        // If already in chart mode, the only control is to toggle back to table mode.
        LogSubMode::Chart => {
            if let KeyCode::Char('c') = key_event.code {
                app.logmode.submode = LogSubMode::Table;
            }
        }
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    mut main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
    vertical: Layout,
) {
    main_block = main_block
        .title(" Log".bold())
        .title_alignment(Alignment::Center);

    // If in Table submode, show table.
    match app.logmode.submode {
        LogSubMode::Table => {
            let header_footer_style = Style::default().bg(Color::DarkGray);
            let selected_style = Style::default().add_modifier(Modifier::REVERSED);
            let widths = Constraint::from_lengths([12, 12, 12, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8]);

            let mut rows: Vec<Row> = vec![];

            for (i, task_count) in app.logmode.data.clone().into_iter().enumerate() {
                let mut task_count = task_count.as_strings();
                if i == 0 {
                    task_count[0] = "now".to_string();
                }
                let mut new_row = vec![];
                for (i, each) in task_count.into_iter().enumerate() {
                    if i == 0 {
                        new_row.push(Text::from(each));
                    } else {
                        new_row.push(Text::from(each).alignment(Alignment::Right))
                    }
                }
                rows.push(Row::new(new_row));
            }

            let mut table = Table::new(rows, widths)
                .header(Row::new(TaskCount::labels()).style(header_footer_style))
                .row_highlight_style(selected_style)
                .flex(ratatui::layout::Flex::SpaceBetween);

            // If more than x rows, add footer.
            if app.logmode.data.len() > 20 {
                table = table.footer(Row::new(TaskCount::labels()).style(header_footer_style));
            }
            frame.render_stateful_widget(table.block(main_block), left, &mut app.logmode.table);
        }
        LogSubMode::Chart => {
            let today = Local::now().date_naive();
            // Create the datasets for the chart.
            /*
            A couple of Ratatui limitations are being worked around here:
              1. Datasets can only be an array of f64 tuples, so using the date
              for the x-axis, as I'd prefer, is not possible. Instead, I'm getting days
              since today (though then using that to get date for the label).
              2. The axes only go from lower number to larger number. So while the label will be positive, the days are actually negative so that the graph
              goes correctly from longer ago to more recent.
            */
            let incomplete = app
                .logmode
                .data
                .clone()
                .into_iter()
                .map(|tc| (-(today - tc.date).num_days() as f64, tc.incomplete as f64))
                .collect::<Vec<(f64, f64)>>();

            let complete = app
                .logmode
                .data
                .clone()
                .into_iter()
                .map(|tc| (-(today - tc.date).num_days() as f64, tc.complete as f64))
                .collect::<Vec<(f64, f64)>>()
                .clone();

            let datasets = vec![
                Dataset::default()
                    .name("incomplete tasks")
                    .marker(symbols::Marker::Braille)
                    .graph_type(GraphType::Line)
                    .style(Style::default().cyan())
                    .data(&incomplete),
                Dataset::default()
                    .name("completed tasks")
                    .marker(symbols::Marker::Braille)
                    .graph_type(GraphType::Line)
                    .style(ratatui::style::Stylize::magenta(Style::default()))
                    .data(&complete),
            ];

            // Create the axes.
            // Max bound for x axis the number of days from today to oldest entry, which
            // has already been calculated above.
            let x_max = complete.last().unwrap().0.abs();
            let x_mid = (x_max / 2.0).round();
            let x_max_label = today
                .checked_sub_days(Days::new(x_max as u64))
                .unwrap()
                .to_string();
            let x_mid_label = today
                .checked_sub_days(Days::new(x_mid as u64))
                .unwrap()
                .to_string();

            // Determine max bound of y axis (highest number of tasks).
            // Get max incomplete tasks.
            let incomplete_max = incomplete
                .clone()
                .into_iter()
                .map(|(_, y)| y as u64)
                .max()
                .unwrap();
            // Get max completed tasks.
            let complete_max = complete
                .clone()
                .into_iter()
                .map(|(_, y)| y as u64)
                .max()
                .unwrap();
            // Get the max of those.
            let y_max = incomplete_max.max(complete_max) as f64;
            let y_max_label = y_max.to_string();

            // Determine min bound of y axis (lowest number of tasks).
            // Get mmin incomplete tasks.
            let incomplete_min = incomplete
                .clone()
                .into_iter()
                .map(|(_, y)| y as u64)
                .min()
                .unwrap();
            // Get max completed tasks.
            let complete_min = complete
                .clone()
                .into_iter()
                .map(|(_, y)| y as u64)
                .min()
                .unwrap();
            // Get the min of those.
            let y_min = incomplete_min.min(complete_min) as f64;
            let y_min_label = y_min.to_string();
            // Get the midway point.
            let y_mid = (y_max - y_min).round() as u64 / 2;
            let y_mid_label = y_mid.to_string();

            let x_axis = Axis::default()
                .title("Date".light_blue())
                .style(Style::default().white())
                .bounds([-x_max, 0.0])
                .labels([x_max_label, x_mid_label, today.to_string()])
                .labels_alignment(Alignment::Center);

            let y_axis = Axis::default()
                .title("Number of tasks".light_blue())
                .style(Style::default().white())
                .bounds([y_min, y_max])
                .labels([y_min_label, y_mid_label, y_max_label]);

            // Create and render the chart.
            let chart = Chart::new(datasets)
                .x_axis(x_axis)
                .y_axis(y_axis)
                .legend_position(Some(LegendPosition::TopLeft));
            frame.render_widget(chart.block(main_block), left);
        }
    }

    info_block = info_block.title_top(Line::from(" Changes ").centered());
    if app.logmode.data.len() < 2 {
        frame.render_widget(
            Paragraph::new(vec![Line::from(
                "Changes will appear here when you have multiple dates of data.",
            )])
            .wrap(Wrap { trim: false })
            .block(info_block),
            top_right,
        )
    } else if let Some(v) = app.logmode.table.selected() {
        let selected_record = app.logmode.data[v].clone();
        let now = app.logmode.data[0].clone();

        // Get no. days b/t today & selected record, to show in 2nd set of changes.
        let todays_date = Local::now().date_naive();
        let selected_date = if v == 0 {
            todays_date
        } else {
            NaiveDate::parse_from_str(&selected_record.as_strings()[0], "%Y-%m-%d").unwrap()
        };
        let days = (todays_date - selected_date).num_days();

        // Diff number of tasks between today and selected record.
        let selected_ints = selected_record
            .as_strings()
            .iter()
            .skip(1)
            .map(|count| count.parse().unwrap())
            .collect::<Vec<i32>>();

        let now_ints = now
            .as_strings()
            .iter()
            .skip(1)
            .map(|count| count.parse().unwrap())
            .collect::<Vec<i32>>();

        let mut diff_ints_now = vec![];
        for i in 0..selected_ints.len() {
            diff_ints_now.push(format!("{}", now_ints[i] - selected_ints[i]));
        }

        // The part with differences between previous record will be different depending
        // on which log record is selected.
        let to_prev: Box<dyn WidgetRef> = if v != app.logmode.data.len() - 1 {
            // Only do diffs with previous date if not oldest log entry.
            let previous = app.logmode.data[v + 1].clone();
            let previous_ints = previous
                .as_strings()
                .iter()
                .skip(1)
                .map(|count| count.parse().unwrap())
                .collect::<Vec<i32>>();
            let mut diff_ints_prev = vec![];
            for i in 0..selected_ints.len() {
                diff_ints_prev.push(format!("{}", selected_ints[i] - previous_ints[i]));
            }
            Box::new(Table::new(
                vec![
                    Row::new(vec![
                        Text::from("To previous").alignment(Alignment::Left),
                        Text::from("complete").alignment(Alignment::Right),
                        Text::from("incomplete").alignment(Alignment::Right),
                    ])
                    .underlined(),
                    // Changes between current and previous periods.
                    Row::new(vec![
                        Text::from("total").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[1].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[0].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("priority 1").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[3].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[2].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("priority 2").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[5].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[4].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("priority 3").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[7].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[6].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("priority 4").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[9].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[8].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("priority 5").alignment(Alignment::Left),
                        Text::from(diff_ints_prev[11].clone()).alignment(Alignment::Right),
                        Text::from(diff_ints_prev[10].clone()).alignment(Alignment::Right),
                    ]),
                    Row::new(vec![
                        Text::from("").alignment(Alignment::Left),
                        Text::from("").alignment(Alignment::Left),
                        Text::from("").alignment(Alignment::Left),
                    ]),
                ],
                &[
                    Constraint::Length(13),
                    Constraint::Length(10),
                    Constraint::Length(10),
                ],
            ))
        // Just display message if no prior log entry to compare to.
        } else {
            Box::new(
                Paragraph::new(vec![Line::from(
                    "At start - no previous entry to compare to.",
                )])
                .wrap(Wrap { trim: false }),
            )
        };

        let to_now = Table::new(
            vec![
                Row::new(vec![
                    Text::from(format!("{days} days ago")).alignment(Alignment::Left),
                    Text::from("complete").alignment(Alignment::Right),
                    Text::from("incomplete").alignment(Alignment::Right),
                ])
                .underlined(),
                Row::new(vec![
                    Text::from("total").alignment(Alignment::Left),
                    Text::from(diff_ints_now[1].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[0].clone()).alignment(Alignment::Right),
                ]),
                Row::new(vec![
                    Text::from("priority 1").alignment(Alignment::Left),
                    Text::from(diff_ints_now[3].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[2].clone()).alignment(Alignment::Right),
                ]),
                Row::new(vec![
                    Text::from("priority 2").alignment(Alignment::Left),
                    Text::from(diff_ints_now[5].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[4].clone()).alignment(Alignment::Right),
                ]),
                Row::new(vec![
                    Text::from("priority 3").alignment(Alignment::Left),
                    Text::from(diff_ints_now[7].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[6].clone()).alignment(Alignment::Right),
                ]),
                Row::new(vec![
                    Text::from("priority 4").alignment(Alignment::Left),
                    Text::from(diff_ints_now[9].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[8].clone()).alignment(Alignment::Right),
                ]),
                Row::new(vec![
                    Text::from("priority 5").alignment(Alignment::Left),
                    Text::from(diff_ints_now[11].clone()).alignment(Alignment::Right),
                    Text::from(diff_ints_now[10].clone()).alignment(Alignment::Right),
                ]),
            ],
            [
                Constraint::Length(13),
                Constraint::Length(10),
                Constraint::Length(10),
            ],
        );

        // Split the info box into two more areas, one for Table and one for Paragraph.
        let [mut info_top, mut info_bottom] = vertical.areas(top_right);

        // Add margins so they fit within top right, rather than overlap it.
        let margin = Margin::new(1, 1);
        info_top = info_top.inner(margin);
        info_bottom = info_bottom.inner(margin);

        // Adjust the size/positions.
        info_top.height = 9;
        info_bottom.y = info_top.bottom() - 1;

        // Finally, render it.
        frame.render_widget(info_block, top_right);
        to_prev.render_ref(info_top, frame.buffer_mut());
        to_now.render_ref(info_bottom, frame.buffer_mut());
    }

    // Controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Scroll up 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "↓".blue(),
            " or ".into(),
            "j".blue(),
            " Scroll down 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["ESC".blue(), " Go to top of log".into()].into());
    controls_content.push(vec!["c".blue(), " Chart tasks".into()].into());
}
