#![forbid(unsafe_code)]
//! View and browse individual tasks from all files.

use std::io::Stdout;

use chrono::Local;
use crossterm::{
    event::{KeyCode, KeyEvent},
    execute,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    Frame, Terminal,
    layout::{Alignment, Constraint, Rect},
    prelude::CrosstermBackend,
    style::{Color, Modifier, Style, Stylize},
    text::{Line, Text},
    widgets::{Block, Paragraph, Row, Table, TableState, Wrap},
};

use crate::{App, CompletionStatus, FileStatus, RichTask, TfError, edit};

pub struct TaskMode {
    pub data: Vec<RichTask>,
    pub table: TableState,
    pub file_status: FileStatus,
    pub completion_status: CompletionStatus,
}

/// Handle user input in task mode.
pub fn handle_input(
    app: &mut App,
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    key_event: KeyEvent,
) -> Result<(), TfError> {
    let num_rows = app.taskmode.data.len();
    match key_event.code {
        KeyCode::Char('j') | KeyCode::Down => {
            let i = match app.taskmode.table.selected() {
                Some(i) => {
                    if i >= num_rows - 1 {
                        0
                    } else {
                        i + 1
                    }
                }
                None => 0,
            };
            app.taskmode.table.select(Some(i));
        }
        KeyCode::Char('k') | KeyCode::Up => {
            let i = match app.taskmode.table.selected() {
                Some(i) => {
                    if i == 0 {
                        num_rows - 1
                    } else {
                        i - 1
                    }
                }
                None => 0,
            };
            app.taskmode.table.select(Some(i));
        }
        KeyCode::Char('c') => {
            app.taskmode.completion_status = match app.taskmode.completion_status {
                CompletionStatus::Incomplete => CompletionStatus::Completed,
                CompletionStatus::Completed => CompletionStatus::Incomplete,
            };
            app.taskmode.data = RichTask::collect(app)?;
            if !app.taskmode.data.is_empty() {
                app.taskmode.table.select(Some(0));
            }
        }
        KeyCode::Char('a') => {
            app.taskmode.file_status = match app.taskmode.file_status {
                FileStatus::Stale => FileStatus::Active,
                FileStatus::Active => FileStatus::Archived,
                FileStatus::Archived => FileStatus::Active,
            };
            app.taskmode.data = RichTask::collect(app)?;
            if !app.taskmode.data.is_empty() {
                app.taskmode.table.select(Some(0));
            }
        }
        KeyCode::Char('s') => {
            app.taskmode.file_status = match app.taskmode.file_status {
                FileStatus::Stale => FileStatus::Active,
                FileStatus::Active => FileStatus::Stale,
                FileStatus::Archived => FileStatus::Stale,
            };
            app.taskmode.data = RichTask::collect(app)?;
            if !app.taskmode.data.is_empty() {
                app.taskmode.table.select(Some(0));
            }
        }
        KeyCode::Enter => {
            if !app.taskmode.data.is_empty() {
                if let Some(v) = app.taskmode.table.selected() {
                    let file = app.taskmode.data[v].file_path.clone();
                    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
                    edit(file)?;
                    execute!(terminal.backend_mut(), EnterAlternateScreen)?;
                    // Force redraw of everything.
                    terminal.clear()?;
                    // Refresh tasks in case any changes were made.
                    app.taskmode.data = RichTask::collect(app)?;
                }
            }
        }
        KeyCode::Esc => app.taskmode.table = TableState::default().with_selected(0),
        _ => (),
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    mut main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
) {
    // Put content into the info block - top right area.
    info_block = info_block.title_top(Line::from(" Status ").centered());

    let mut info_content = vec![];

    let completion_status = match app.taskmode.completion_status {
        CompletionStatus::Incomplete => "incomplete",
        CompletionStatus::Completed => "completed",
    };

    let file_status = match app.taskmode.file_status {
        FileStatus::Active => "active",
        FileStatus::Archived => "archived",
        FileStatus::Stale => "stale",
    };

    info_content.push(Line::from(vec![
        "Viewing ".into(),
        format!("{completion_status} ").green(),
        "tasks from ".into(),
        format!("{file_status} ").green(),
        "files.".into(),
    ]));
    info_content.push(Line::from(""));

    if !app.taskmode.data.is_empty() {
        if let Some(v) = app.taskmode.table.selected() {
            let task = app.taskmode.data[v].clone();
            info_content.push(Line::from(format!("File: {}", task.file_name)));
            info_content.push(Line::from(format!("Task set: {}", task.task_set)));
        };
    }

    frame.render_widget(
        Paragraph::new(info_content)
            .wrap(Wrap { trim: false })
            .block(info_block),
        top_right,
    );

    // Put content into main area.
    main_block = main_block
        .title(" Tasks".bold())
        .title_alignment(Alignment::Center);

    let header_style = Style::default().bg(Color::DarkGray);
    let selected_style = Style::default().add_modifier(Modifier::REVERSED);
    let widths = vec![
        Constraint::Min(9),
        Constraint::Percentage(100),
        Constraint::Min(11),
        Constraint::Min(11),
    ];

    let mut rows: Vec<Row> = vec![];
    if app.taskmode.data.is_empty() {
        rows.push(Row::new([Text::from(""), Text::from("No tasks to view")]))
    } else {
        let today = Local::now().date_naive();
        for task in &mut app.taskmode.data {
            let due_date = if let Some(v) = task.task.due_date {
                let text = Text::from(format!("{v}"));
                if !task.task.completed {
                    if today > v { text.red() } else { text }
                } else {
                    text
                }
            } else {
                Text::from("")
            };

            let completed_date = if let Some(v) = task.task.completed_date {
                Text::from(format!("{v}"))
            } else {
                Text::from("")
            };
            rows.push(Row::new(vec![
                Text::from(format!("{}", task.task.priority)),
                Text::from(task.task.text.clone()),
                due_date,
                completed_date,
            ]));
        }
    }

    let table = Table::new(rows, widths)
        .header(
            Row::new(vec![
                Text::from("priority"),
                Text::from("task name"),
                Text::from("due"),
                Text::from("completed"),
            ])
            .style(header_style),
        )
        .row_highlight_style(selected_style)
        .flex(ratatui::layout::Flex::SpaceBetween);

    frame.render_stateful_widget(table.block(main_block), left, &mut app.taskmode.table);

    // Add controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Previous task".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["↓".blue(), " or ".into(), "j".blue(), " Next task".into()].into());
    controls_content.push(vec!["Enter".blue(), " Edit task's file".into()].into());
    controls_content.push(vec!["Esc".blue(), " Go to top".into()].into());
    controls_content.push(Line::from(""));
    controls_content.push(vec!["Filter tasks by".blue().bold().underlined()].into());
    controls_content.push(vec!["c".blue(), " completion status".into()].into());
    controls_content.push(vec!["a".blue(), " archived status".into()].into());
    controls_content.push(vec!["s".blue(), " staleness".into()].into());
}
