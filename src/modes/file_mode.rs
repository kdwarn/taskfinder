#![forbid(unsafe_code)]
//! Browse and edit files with tasks.

use std::cmp::Reverse;
use std::io::Stdout;

use chrono::Local;
use crossterm::{
    event::{KeyCode, KeyEvent, KeyModifiers},
    execute,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    Frame, Terminal,
    backend::CrosstermBackend,
    layout::{Alignment, Constraint, Layout, Margin, Rect},
    style::{Color, Style, Stylize},
    text::{Line, Span, Text},
    widgets::{Block, Borders, Clear, Paragraph, Row, Table, Wrap},
};

use crate::{
    App, FileStatus, FileWithTasks, Mode, Priority, SIDEBAR_SIZE, TfError, centered_rect,
    count::TaskCount, dialog::Dialog, edit,
};

pub struct FileMode {
    pub current_file: usize,
    pub files: Vec<FileWithTasks>,
    pub completed: bool,
    pub due: bool,
    pub file_status: FileStatus,
    pub priority: Option<Priority>,
    pub tag_dialog: Dialog,
    pub line_offset: u16,
}

/// Handle user input in file mode.
pub fn handle_input(
    app: &mut App,
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    key_event: KeyEvent,
) -> Result<(), TfError> {
    if !app.filemode.tag_dialog.up {
        match key_event.code {
            KeyCode::Char('j') | KeyCode::Down => app.filemode.line_offset += 1,
            KeyCode::Char('k') | KeyCode::Up => {
                if app.filemode.line_offset > 0 {
                    app.filemode.line_offset -= 1;
                }
            }
            KeyCode::PageDown => app.filemode.line_offset += 20,
            KeyCode::PageUp => {
                if app.filemode.line_offset >= 20 {
                    app.filemode.line_offset -= 20;
                } else {
                    app.filemode.line_offset = 0;
                }
            }
            KeyCode::Char('n') | KeyCode::Right => {
                if app.filemode.files.is_empty() {
                    app.filemode.current_file = 0;
                } else if app.filemode.current_file == app.filemode.files.len() - 1 {
                    app.filemode.current_file = 0
                } else {
                    app.filemode.current_file += 1;
                }
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('p') | KeyCode::Left => {
                if app.filemode.files.is_empty() {
                    app.filemode.current_file = 0;
                } else if app.filemode.current_file == 0 {
                    app.filemode.current_file = app.filemode.files.len() - 1;
                } else {
                    app.filemode.current_file -= 1;
                }
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('d') => {
                if key_event.modifiers == KeyModifiers::CONTROL {
                    app.filemode.line_offset += 20;
                } else {
                    app.filemode.due = !app.filemode.due;
                    app.filemode.current_file = 0;
                    app.filemode.line_offset = 0;
                    refine_files(app)?;
                }
            }
            KeyCode::Char('u') => {
                if key_event.modifiers == KeyModifiers::CONTROL {
                    if app.filemode.line_offset >= 20 {
                        app.filemode.line_offset -= 20;
                    } else {
                        app.filemode.line_offset = 0;
                    }
                }
            }
            KeyCode::Enter => {
                // Because edit calls another terminal program,
                // leave and enter alternate screen around it to ensure that
                // the screen gets cleared properly.
                // (Without this, there are remnants of taskfinder's
                // interface left over after exiting if a file is opened
                // at any point during its use.)
                if !app.filemode.files.is_empty() {
                    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
                    edit(app.filemode.files[app.filemode.current_file].file.clone())?;
                    execute!(terminal.backend_mut(), EnterAlternateScreen)?;
                    // Force redraw of everything.
                    terminal.clear()?;
                    app.filemode.current_file = 0;
                    refine_files(app)?;
                    app.filemode.line_offset = 0;
                }
            }
            KeyCode::Char('1') => {
                filter_by_priority(app, Priority::One);
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('2') => {
                filter_by_priority(app, Priority::Two);
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('3') => {
                filter_by_priority(app, Priority::Three);
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('4') => {
                filter_by_priority(app, Priority::Four);
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('5') => {
                filter_by_priority(app, Priority::Five);
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('/') => app.filemode.tag_dialog.up = true,
            KeyCode::Char('c') => {
                app.filemode.completed = !app.filemode.completed;
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('s') => {
                app.filemode.current_file = 0;
                app.filemode.file_status = match app.filemode.file_status {
                    FileStatus::Stale => FileStatus::Active,
                    FileStatus::Active => FileStatus::Stale,
                    FileStatus::Archived => FileStatus::Stale,
                };
                app.filemode.completed = true;
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Char('a') => {
                app.filemode.current_file = 0;
                app.filemode.file_status = match app.filemode.file_status {
                    FileStatus::Archived => FileStatus::Active,
                    FileStatus::Stale => FileStatus::Archived,
                    FileStatus::Active => FileStatus::Archived,
                };
                refine_files(app)?;
                app.filemode.line_offset = 0;
            }
            KeyCode::Esc => {
                // Reset mode to defaults.
                app.filemode.completed = app.config.include_completed;
                app.filemode.due = false;
                app.filemode.file_status = FileStatus::Active;
                app.filemode.priority = None;
                app.filemode.current_file = 0;
                app.filemode.line_offset = 0;
                app.filemode.tag_dialog.reset();
                refine_files(app)?;
            }
            _ => (),
        }
    } else {
        // Tag dialog.
        match key_event.code {
            KeyCode::Enter => {
                app.filemode.tag_dialog.up = false;
                app.filemode.current_file = 0;
                app.filemode.line_offset = 0;
                // Move working_input to submitted_input, setting working_input to default
                app.filemode.tag_dialog.submitted_input =
                    std::mem::take(&mut app.filemode.tag_dialog.working_input);
                app.filemode.tag_dialog.submitted_input.trim().to_string();
                app.filemode.tag_dialog.cursor_position = 0;
                app.mode = Mode::File;
                refine_files(app)?;
            }
            KeyCode::Char(to_insert) => app.filemode.tag_dialog.insert_char(to_insert),
            KeyCode::Backspace => app.filemode.tag_dialog.backspace(),
            KeyCode::Delete => app.filemode.tag_dialog.delete(),
            KeyCode::End => app.filemode.tag_dialog.end(),
            KeyCode::Home => app.filemode.tag_dialog.home(),
            KeyCode::Left => app.filemode.tag_dialog.move_cursor_left(),
            KeyCode::Right => app.filemode.tag_dialog.move_cursor_right(),
            KeyCode::Esc => {
                app.filemode.tag_dialog.up = false;
                app.filemode.tag_dialog.reset();
            }
            _ => (),
        }
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
    vertical: Layout,
) {
    let main_text = if app.filemode.files.is_empty() {
        vec![Line::from("No files/tasks to show.")]
    } else {
        render_file(app, app.filemode.files[app.filemode.current_file].clone())
    };
    frame.render_widget(
        Paragraph::new(main_text)
            .wrap(Wrap { trim: false })
            .scroll((app.filemode.line_offset, 0))
            .block(main_block),
        left,
    );

    info_block = info_block.title_top(Line::from(" Status ").centered());
    let highlight_style = Style::new().fg(Color::Green);

    let tag_status = Line::from(if app.filemode.tag_dialog.submitted_input.is_empty() {
        vec![
            Span::raw("  *"),
            Span::styled(" any ", highlight_style),
            Span::raw("tag"),
        ]
    } else {
        vec![
            Span::raw("  *"),
            Span::styled(
                format!(" {} ", app.filemode.tag_dialog.submitted_input),
                highlight_style,
            ),
            Span::raw("tag"),
        ]
    });

    let completed_status = Line::from(if app.filemode.completed {
        vec![
            Span::raw("  * incomplete"),
            Span::styled(" or completed ", highlight_style),
        ]
    } else {
        vec![
            Span::raw("  *"),
            Span::styled(" incomplete ", highlight_style),
            Span::raw("only"),
        ]
    });

    let priority_status = Line::from(if let Some(v) = &app.filemode.priority {
        vec![
            Span::raw("  *"),
            Span::styled(format!(" {} ", v), highlight_style),
            Span::raw("priority level"),
        ]
    } else {
        vec![
            Span::raw("  *"),
            Span::styled(" any ", highlight_style),
            Span::raw("priority level"),
        ]
    });

    let active_status = Line::from(vec![
        Span::raw("  * from"),
        Span::styled(
            match app.filemode.file_status {
                FileStatus::Stale => " stale ",
                FileStatus::Archived => " archived ",
                FileStatus::Active => " active ",
            },
            highlight_style,
        ),
        Span::raw("files"),
    ]);

    let due_status = Line::from(if app.filemode.due {
        vec![
            Span::raw("  *"),
            Span::styled(" with ", highlight_style),
            Span::raw("due dates only"),
        ]
    } else {
        vec![
            Span::raw("  *"),
            Span::styled(" with or without ", highlight_style),
            Span::raw("due dates"),
        ]
    });

    let filter_text = Paragraph::new(vec![
        Line::from("Current filters:").underlined(),
        priority_status,
        tag_status,
        completed_status,
        due_status,
        active_status,
    ]);

    let all_files_count = TaskCount::extract(&app.filemode.files, &app.filemode.file_status);

    let (file_count_complete, file_count_incomplete) = if !app.filemode.files.is_empty() {
        let file_count = TaskCount::extract(
            &[app.filemode.files[app.filemode.current_file].clone()],
            &app.filemode.file_status,
        );
        (file_count.complete, file_count.incomplete)
    } else {
        (0, 0)
    };

    let rows = [
        Row::new(vec![
            Text::from("         "),
            Text::from("complete").alignment(Alignment::Right),
            Text::from("incomplete").alignment(Alignment::Right),
        ]),
        Row::new(vec![
            Text::from("All files:"),
            Text::from(format!("{}", all_files_count.complete)).alignment(Alignment::Right),
            Text::from(format!("{}", all_files_count.incomplete)).alignment(Alignment::Right),
        ]),
        Row::new(vec![
            Text::from("Current file:"),
            Text::from(format!("{}", file_count_complete)).alignment(Alignment::Right),
            Text::from(format!("{}", file_count_incomplete)).alignment(Alignment::Right),
        ]),
    ];

    let count_table = Table::new(
        rows,
        &[
            Constraint::Length(14),
            Constraint::Length(9),
            Constraint::Length(10),
        ],
    );

    // Split the info box into two more areas, one for Table and one for Paragraph.
    let [mut info_top, mut info_bottom] = vertical.areas(top_right);

    // Add margins so they fit within top right, rather than overlap it.
    let margin = Margin::new(1, 1);
    info_top = info_top.inner(margin);
    info_bottom = info_bottom.inner(margin);

    // Adjust the size/positions.
    info_top.height = 8;
    info_bottom.y = info_top.bottom() - 1;

    frame.render_widget(info_block, top_right);
    frame.render_widget(filter_text.wrap(Wrap { trim: false }), info_top);
    frame.render_widget(
        count_table.block(Block::new().title("Number of tasks:".underlined())),
        info_bottom,
    );

    if app.filemode.tag_dialog.up {
        let dialog_block = Block::default()
            .title("Enter tag to filter by:")
            .title_bottom("Press Enter to submit or Esc to abort")
            .title_alignment(Alignment::Center)
            .borders(Borders::ALL)
            .style(Style::default().bg(Color::DarkGray));
        let area = centered_rect(45, 5, frame.area(), SIDEBAR_SIZE);

        frame.render_widget(Clear, area);
        frame.render_widget(
            Paragraph::new(app.filemode.tag_dialog.render_working_input()).block(dialog_block),
            area,
        );
    }

    // Controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(vec!["→".blue(), " or ".into(), "n".blue(), " Next file".into()].into());
    controls_content.push(
        vec![
            "←".blue(),
            " or ".into(),
            "p".blue(),
            " Previous file".into(),
        ]
        .into(),
    );

    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Scroll up 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "↓".blue(),
            " or ".into(),
            "j".blue(),
            " Scroll down 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgDn".blue(),
            " or ".into(),
            "CTRL+d".blue(),
            " Scroll down 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgUp".blue(),
            " or ".into(),
            "CTRL+u".blue(),
            " Scroll up 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["Enter".blue(), " Edit file".into()].into());
    controls_content.push(Line::from(""));
    controls_content.push(vec!["Filter files/tasks by".blue().bold().underlined()].into());
    controls_content.push(vec!["1 2 3 4 5".blue(), " priority".into()].into());
    controls_content.push(vec!["/".blue(), " tag".into()].into());
    controls_content.push(vec!["c".blue(), " completion status".into()].into());
    controls_content.push(vec!["d".blue(), " due date".into()].into());
    controls_content.push(vec!["s".blue(), " staleness".into()].into());
    controls_content.push(vec!["a".blue(), " archived status".into()].into());
    controls_content.push(vec!["Esc".blue(), " Clear all ".into()].into());
    controls_content.push(Line::from(""));
}
/// Filter/retain and sort based on App state.
pub fn refine_files(app: &mut App) -> Result<(), TfError> {
    // Retain only those files/tasks sections that have a specific priority.
    app.filemode.files = FileWithTasks::collect(&app.config)?;

    // Filter based on whether a due date is included in tasks or not.
    if app.filemode.due {
        app.filemode.files.retain(|file| file.has_due_date);
        for file in app.filemode.files.iter_mut() {
            for task_set in file.task_sets.iter_mut() {
                task_set.tasks.retain(|task| task.due_date.is_some())
            }
        }
    }

    // Filter by tag.
    if !app.filemode.tag_dialog.submitted_input.is_empty() {
        app.filemode.files.retain(|file| {
            file.head
                .join("")
                .to_lowercase()
                .contains(&app.filemode.tag_dialog.submitted_input.to_lowercase())
        })
    }

    // Filter based on priority
    if let Some(v) = &app.filemode.priority {
        for file in app.filemode.files.iter_mut() {
            // If there's a file-level matching priority, keep any task sections that have
            // either the same priority or no priority.
            if file.priority == *v {
                file.task_sets.retain(|task_set| {
                    task_set.priority == *v || task_set.priority == Priority::NoPriority
                })
            // If the file-level priority doesn't match, keep all sets that have matching priority.
            } else {
                file.task_sets.retain(|task_set| task_set.priority == *v)
            }
        }
    }

    // Filter on FileStatus
    match app.filemode.file_status {
        FileStatus::Stale => app
            .filemode
            .files
            .retain(|file| matches!(file.status, FileStatus::Stale)),
        FileStatus::Archived => app
            .filemode
            .files
            .retain(|file| matches!(file.status, FileStatus::Archived)),
        FileStatus::Active => app
            .filemode
            .files
            .retain(|file| matches!(file.status, FileStatus::Active)),
    };

    // Retain only those files that still have tasks sets after the above.
    app.filemode.files.retain(|file| !file.task_sets.is_empty());

    // Sort: first by file-level and task-set-level priority (high to low)
    // and then by file modification (latest to oldest).
    app.filemode.files.sort_by_key(|file| {
        // Start with file-level priority, but if one of its task sets which has an incomplete
        // task has a higher priority, use it instead.
        let mut priority = file.priority;
        for task_set in file.task_sets.iter() {
            if task_set.priority > priority && task_set.tasks.iter().any(|task| !task.completed) {
                priority = task_set.priority;
            }
        }
        (Reverse(priority), Reverse(file.last_modified))
    });

    Ok(())
}

fn filter_by_priority(app: &mut App, priority: Priority) {
    app.filemode.current_file = 0;
    // Toggle it to no priority if selecting the same one.
    if app.filemode.priority == Some(priority) {
        app.filemode.priority = None;
    } else {
        app.filemode.priority = Some(priority);
    }
}

pub fn render_file(app: &App, mut f: FileWithTasks) -> Vec<Line> {
    let light_blue_bold = Style::new().light_blue().bold();

    let mut text = vec![];

    text.push(Line::from(format!(
        "File {} of {}",
        app.filemode.current_file + 1,
        app.filemode.files.len()
    )));
    text.push(Line::from(f.file.display().to_string()));
    text.push(Line::from(f.head[0].clone()).style(light_blue_bold));
    text.push(Line::from(f.head[1].clone()).style(light_blue_bold));

    // If user only wants to see incomplete tasks, and there are none, notify them of this.
    if !app.filemode.completed
        && f.task_sets
            .iter()
            .all(|t| t.tasks.iter().all(|task| task.completed))
    {
        text.push("".into());
        text.push("No incomplete tasks.".into());
        return text;
    }

    // Sort tasks sets by priority
    f.task_sets
        .sort_unstable_by_key(|task_set| Reverse(task_set.priority));

    for task_set in f.task_sets {
        // Show (or not) task section header.
        if !task_set.tasks.is_empty() {
            // Dim the header if all tasks within the task set are complete.
            // (So long as the user wants to include used tasks in the display, and if they
            // don't, then don't add anything to display.)
            if task_set.tasks.iter().all(|t| t.completed) {
                if app.filemode.completed {
                    text.push(Line::from(""));
                    text.push(Line::from(task_set.section).style(Style::new().underlined().dim()))
                }
            } else {
                text.push(Line::from(""));
                text.push(
                    Line::from(task_set.section).style(Style::new().underlined().light_blue()),
                )
            }
        }

        if !task_set.tasks.is_empty() {
            for task in task_set.tasks {
                // Get and format due/completed dates.
                let due_date = if let Some(v) = task.due_date {
                    let current_date = Local::now().date_naive();
                    if v < current_date && !task.completed {
                        Span::styled(format!("due:{}", v), Style::new().red())
                    } else {
                        Span::styled(format!("due:{}", v), Style::new().green())
                    }
                } else {
                    Span::raw("")
                };
                let completed_date = if let Some(v) = task.completed_date {
                    Span::styled(format!("completed:{v}"), Style::new().magenta())
                } else {
                    Span::raw("")
                };
                // Include complete tasks only if user wants them.
                if app.filemode.completed && task.completed {
                    text.push(
                        Line::from(vec![
                            Span::raw(task.text),
                            Span::raw(" "),
                            due_date,
                            Span::raw(" "),
                            completed_date,
                        ])
                        .style(Style::new().dim()),
                    )
                } else if !task.completed {
                    text.push(Line::from(vec![
                        Span::raw(task.text),
                        Span::raw(" "),
                        due_date,
                    ]))
                }
            }
        }
    }
    text
}
