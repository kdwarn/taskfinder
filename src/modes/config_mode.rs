#![forbid(unsafe_code)]
//! Allow the user to change configuration settings.

use std::path::Path;

use crossterm::event::{KeyCode, KeyEvent};
use ratatui::{
    Frame,
    layout::{Alignment, Constraint, Rect},
    style::{Color, Modifier, Style, Stylize},
    text::{Line, Text},
    widgets::{Block, Borders, Clear, Paragraph, Row, Table, TableState, Wrap},
};

use crate::{
    App, SIDEBAR_SIZE, TfError, centered_rect,
    config::{self, Config, ConfigError},
    dialog::Dialog,
    modes::file_mode,
};

pub struct ConfigMode {
    pub table: TableState,
    pub setting: Option<ConfigSetting>,
    pub dialog: Dialog,
    pub message_to_user: Option<String>,
}

/// User-editable settings in config file.
///
/// num_tasks_log is not included.
#[derive(Debug, Clone)]
pub enum ConfigSetting {
    Path,
    FileExtensions,
    DaysToStale,
    IncludeComplete,
    PriorityMarkers,
    EvergreenFile,
}

impl ConfigSetting {
    pub fn get(num: usize) -> Option<Self> {
        match num {
            0 => Some(ConfigSetting::Path),
            1 => Some(ConfigSetting::FileExtensions),
            2 => Some(ConfigSetting::DaysToStale),
            3 => Some(ConfigSetting::IncludeComplete),
            4 => Some(ConfigSetting::PriorityMarkers),
            5 => Some(ConfigSetting::EvergreenFile),
            _ => None,
        }
    }

    pub fn help_text(&self) -> &'static str {
        match self {
            Self::Path => {
                "The path to your files and tasks. It will be verified before being saved."
            }
            Self::FileExtensions => "The extensions of files to include.",
            Self::DaysToStale => {
                "Number of days since a file was last modified to consider its tasks stale."
            }
            Self::IncludeComplete => {
                "Whether or not to show completed tasks in Files mode by default."
            }
            Self::PriorityMarkers => {
                "The priority markers: the text that indicates the priority of a file or task set, from highest to lowest priority."
            }
            Self::EvergreenFile => {
                "The path to the evergreen file. This will be empty if you are not using such a file. If not empty, the path will be verified before being saved."
            }
        }
    }
}

/// Handle user input in config mode.
pub fn handle_input(app: &mut App, key_event: KeyEvent) -> Result<(), TfError> {
    let num_rows = app.config.table_rows().len();
    app.configmode.message_to_user = None;

    if !app.configmode.dialog.up {
        match key_event.code {
            KeyCode::Char('j') | KeyCode::Down => {
                let i = match app.configmode.table.selected() {
                    Some(i) => {
                        if i >= num_rows - 1 {
                            0
                        } else {
                            i + 1
                        }
                    }
                    None => 0,
                };
                // Set both the row of the TableState and the configmode.setting it corresponds to.
                app.configmode.table.select(Some(i));
                app.configmode.setting = ConfigSetting::get(i);
            }
            KeyCode::Char('k') | KeyCode::Up => {
                let i = match app.configmode.table.selected() {
                    Some(i) => {
                        if i == 0 {
                            num_rows - 1
                        } else {
                            i - 1
                        }
                    }
                    None => 0,
                };
                // Set both the row of the TableState and the configmode.setting it corresponds to.
                app.configmode.table.select(Some(i));
                app.configmode.setting = ConfigSetting::get(i);
            }
            KeyCode::Enter => {
                /*
                    Either change toggle setting or bring up dialog box to get input.
                    In all cases, the current value of the setting is saved to both
                    `configmode.dialog.original` and `configmode.dialog.working_input` - the latter
                    to make the actual changes and the former to later check if there was an actual
                    change made.
                */
                if let Some(num) = app.configmode.table.selected() {
                    if let Some(cs) = ConfigSetting::get(num) {
                        match cs {
                            ConfigSetting::Path => {
                                app.configmode.setting = Some(cs);
                                app.configmode.dialog.up = true;
                                let value = app.config.path.to_string_lossy().to_string();
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                            }
                            ConfigSetting::FileExtensions => {
                                app.configmode.setting = Some(cs);
                                app.configmode.dialog.up = true;
                                let value = app.config.file_extensions.join(",");
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                            }
                            ConfigSetting::DaysToStale => {
                                app.configmode.setting = Some(cs);
                                app.configmode.dialog.up = true;
                                let value = app.config.days_to_stale.to_string();
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                            }
                            // This one is just a toggle, and so immediately submitted.
                            ConfigSetting::IncludeComplete => {
                                app.configmode.setting = Some(cs);
                                let value = app.config.include_completed.to_string();
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                                match submit_config_change(app) {
                                    Ok(v) => {
                                        app.configmode.message_to_user = Some(v);
                                    }
                                    Err(e) => {
                                        app.configmode.message_to_user = Some(format!("{e}"));
                                    }
                                };
                            }
                            ConfigSetting::PriorityMarkers => {
                                app.configmode.setting = Some(cs);
                                app.configmode.dialog.up = true;
                                let value = app.config.priority_markers.join(",");
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                            }
                            ConfigSetting::EvergreenFile => {
                                app.configmode.setting = Some(cs);
                                app.configmode.dialog.up = true;
                                let value = app.config.evergreen_file.to_string_lossy().to_string();
                                app.configmode.dialog.original = value.clone();
                                app.configmode.dialog.working_input = value;
                            }
                        }
                    }
                };
            }
            // Reset selected setting to default value.
            KeyCode::Char('r') => {
                if let Some(num) = app.configmode.table.selected() {
                    if let Some(cs) = ConfigSetting::get(num) {
                        match cs {
                            ConfigSetting::Path => {
                                app.config.path = Config::default_files_path()?;
                                file_mode::refine_files(app)?;
                            }
                            ConfigSetting::FileExtensions => {
                                app.config.file_extensions = Config::default_file_extensions();
                                file_mode::refine_files(app)?;
                            }
                            ConfigSetting::DaysToStale => {
                                app.config.days_to_stale = config::DEFAULT_DAYS_TO_STALE;
                            }
                            ConfigSetting::IncludeComplete => {
                                app.config.include_completed = config::DEFAULT_INCLUDE_COMPLETED;
                            }
                            ConfigSetting::PriorityMarkers => {
                                app.config.priority_markers = Config::default_priority_markers();
                            }
                            ConfigSetting::EvergreenFile => {
                                app.config.evergreen_file = Config::default_evergreen_file();
                            }
                        }
                        app.config.save()?;
                        app.configmode.message_to_user =
                            Some("Reset setting to default.".to_string());
                    }
                };
            }
            _ => (),
        }
    } else {
        // Config dialog.
        match key_event.code {
            KeyCode::Enter => {
                if !matches!(app.configmode.setting, Some(ConfigSetting::IncludeComplete)) {
                    app.configmode.dialog.up = false;
                    // Move working_input to submitted_input, setting working_input to default
                    app.configmode.dialog.submitted_input =
                        std::mem::take(&mut app.configmode.dialog.working_input);
                    app.configmode.dialog.cursor_position = 0;
                    match submit_config_change(app) {
                        Ok(v) => {
                            app.configmode.message_to_user = Some(v);
                        }
                        Err(e) => {
                            app.configmode.message_to_user = Some(format!("{e}"));
                        }
                    }
                }
            }
            KeyCode::Char(to_insert) => app.configmode.dialog.insert_char(to_insert),
            KeyCode::Backspace => app.configmode.dialog.backspace(),
            KeyCode::Delete => app.configmode.dialog.delete(),
            KeyCode::End => app.configmode.dialog.end(),
            KeyCode::Home => app.configmode.dialog.home(),
            KeyCode::Left => app.configmode.dialog.move_cursor_left(),
            KeyCode::Right => app.configmode.dialog.move_cursor_right(),
            KeyCode::Esc => {
                app.configmode.dialog.up = false;
                app.configmode.dialog.reset();
            }
            _ => (),
        }
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    mut main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
) {
    info_block = info_block.title_top(Line::from(" Setting info and result ").centered());

    let mut content = vec![Line::from("")];

    if let Some(v) = &app.configmode.setting {
        content.push(Line::from(v.help_text()));

        if !matches!(v, ConfigSetting::IncludeComplete) {
            content.push(Line::from(""));
            content.push(Line::from(
                "Note: all whitespace will be stripped upon submission.",
            ));
        }
    }

    if let Some(v) = app.configmode.message_to_user.clone() {
        content.push(Line::from(""));
        content.push(Line::from(v))
    }

    frame.render_widget(
        Paragraph::new(content)
            .wrap(Wrap { trim: false })
            .block(info_block),
        top_right,
    );

    main_block = main_block
        .title(" Configuration".bold())
        .title_alignment(Alignment::Center);

    let header_style = Style::default().bg(Color::DarkGray);
    let selected_style = Style::default().add_modifier(Modifier::REVERSED);
    let widths = Constraint::from_lengths([24, 68]);

    let rows = app.config.table_rows();
    let table = Table::new(rows, widths)
        .header(Row::new(vec![Text::from("setting"), Text::from("value")]).style(header_style))
        .row_highlight_style(selected_style)
        .flex(ratatui::layout::Flex::SpaceBetween);

    frame.render_stateful_widget(table.block(main_block), left, &mut app.configmode.table);

    if app.configmode.dialog.up {
        let dialog_block = Block::default()
            .title("Change config setting:")
            .title_bottom("Press Enter to submit or ESC to abort")
            .title_alignment(Alignment::Center)
            .borders(Borders::ALL)
            .style(Style::default().bg(Color::DarkGray));
        let area = centered_rect(60, 5, frame.area(), SIDEBAR_SIZE);

        frame.render_widget(Clear, area);
        frame.render_widget(
            Paragraph::new(app.configmode.dialog.render_working_input()).block(dialog_block),
            area,
        );
    }

    // Controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Previous setting".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "↓".blue(),
            " or ".into(),
            "j".blue(),
            " Next setting".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["Enter".blue(), " Edit/toggle setting".into()].into());
    controls_content.push(vec!["r".blue(), " Reset setting to default".into()].into())
}
/// Try to update the setting, writing it to file if successful.
fn submit_config_change(app: &mut App) -> Result<String, TfError> {
    // Get the submitted input, remove any whitespace at start or end.
    let change = app
        .configmode
        .dialog
        .submitted_input
        .clone()
        .trim()
        .to_string();

    if let Some(ref v) = app.configmode.setting {
        match *v {
            ConfigSetting::Path => {
                let files_path = Path::new(&change).to_path_buf();
                if files_path.exists() {
                    app.config.path = files_path;
                    file_mode::refine_files(app)?;
                } else {
                    return Err(TfError::NoSuchPath);
                }
            }
            ConfigSetting::FileExtensions => {
                app.config.file_extensions = change
                    .split(",")
                    .map(|v| {
                        v.split_whitespace()
                            .fold("".to_string(), |acc, x| format!("{x}{acc}"))
                    })
                    .collect();
                file_mode::refine_files(app)?;
            }
            ConfigSetting::DaysToStale => {
                app.config.days_to_stale = change.parse()?;
            }
            ConfigSetting::IncludeComplete => {
                app.config.include_completed = !app.config.include_completed;
                // Also set current view for this.
                app.filemode.completed = app.config.include_completed;
            }
            ConfigSetting::PriorityMarkers => {
                let new_pms: Vec<String> = change
                    .split(",")
                    .map(|v| {
                        v.split_whitespace()
                            .fold("".to_string(), |acc, x| format!("{x}{acc}"))
                    })
                    .collect();
                if new_pms.len() != 5 {
                    Err(ConfigError::IncorrectNumberOfPriorityMarkers)?;
                } else {
                    app.config.priority_markers = new_pms;
                    // Reset current view for this setting.
                    app.filemode.priority = None;
                    file_mode::refine_files(app)?;
                }
            }
            ConfigSetting::EvergreenFile => {
                let new_evergreen_file = Path::new(&change).to_path_buf();

                if new_evergreen_file.exists() || new_evergreen_file == Path::new("") {
                    app.config.evergreen_file = new_evergreen_file;
                } else {
                    return Err(TfError::NoSuchPath);
                }
            }
        }

        // Write new configuration to file.
        app.config.save()?;
    }
    if change != app.configmode.dialog.original.trim() {
        Ok("Setting changed.".to_string())
    } else {
        Ok("No change made.".to_string())
    }
}
