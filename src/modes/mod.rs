#![forbid(unsafe_code)]
//! The various modes the app can be in.

pub mod config_mode;
pub mod evergreen_mode;
pub mod file_mode;
pub mod help_mode;
pub mod log_mode;
pub mod task_mode;

#[derive(PartialEq, Clone)]
pub enum Mode {
    File,
    Task,
    Log,
    Help,
    Evergreen,
    Config,
}
