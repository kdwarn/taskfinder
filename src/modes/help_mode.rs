#![forbid(unsafe_code)]
//! View help, version, and changelog.

use std::io;

use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};
use ratatui::{
    Frame,
    layout::{Alignment, Rect},
    style::Stylize,
    text::Line,
    widgets::{Block, Paragraph, Wrap},
};

use crate::{App, USAGE};

const CHANGELOG: &str = include_str!("../../CHANGELOG.md");

pub struct HelpMode {
    pub line_offset: u16,
    pub help_text: String,
}

/// Handle user input in log mode.
pub fn handle_input(app: &mut App, key_event: KeyEvent) -> io::Result<()> {
    match key_event.code {
        KeyCode::Char('j') | KeyCode::Down => app.helpmode.line_offset += 1,
        KeyCode::Char('k') | KeyCode::Up => {
            if app.helpmode.line_offset > 0 {
                app.helpmode.line_offset -= 1;
            }
        }
        KeyCode::PageDown => app.helpmode.line_offset += 20,
        KeyCode::PageUp => {
            if app.helpmode.line_offset >= 20 {
                app.helpmode.line_offset -= 20;
            } else {
                app.helpmode.line_offset = 0;
            }
        }
        KeyCode::Char('d') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                app.helpmode.line_offset += 20;
            }
        }
        KeyCode::Char('u') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                if app.helpmode.line_offset >= 20 {
                    app.helpmode.line_offset -= 20;
                } else {
                    app.helpmode.line_offset = 0;
                }
            }
        }
        // Toggle version and changelog with usage.
        KeyCode::Char('v') => {
            let version = std::env!("CARGO_PKG_VERSION");
            let vcl = format!("Taskfinder version {version}\n\n{CHANGELOG}\n",);
            if app.helpmode.help_text == vcl {
                app.helpmode.help_text = USAGE.to_string();
            } else {
                app.helpmode.help_text = vcl;
            }
            app.helpmode.line_offset = 0;
        }
        _ => (),
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    mut main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
) {
    main_block = main_block
        .title(" Help ".bold())
        .title_alignment(Alignment::Center);

    frame.render_widget(
        Paragraph::new(app.helpmode.help_text.clone())
            .wrap(Wrap { trim: false })
            .scroll((app.helpmode.line_offset, 0))
            .block(main_block),
        left,
    );

    info_block = info_block.title_top(Line::from(" Info ").centered());
    frame.render_widget(
                Paragraph::new(vec![Line::from(
                    "Developed by Kris Warner and licensed under the AGPL 3.0 or later. See <https://kdwarn.net>.",
                )])
                .wrap(Wrap { trim: false })
                .block(info_block),
                top_right,
            );

    // Controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Scroll up 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "↓".blue(),
            " or ".into(),
            "j".blue(),
            " Scroll down 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgDn".blue(),
            " or ".into(),
            "CTRL+d".blue(),
            " Scroll down 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgUp".blue(),
            " or ".into(),
            "CTRL+u".blue(),
            " Scroll up 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["v".blue(), " Show version and changelog".into()].into())
}
