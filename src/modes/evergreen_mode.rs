#![forbid(unsafe_code)]
//! View and edit an evergreen file, if configured.

use std::{
    fs,
    io::{self, Stdout},
};

use crossterm::{
    event::{KeyCode, KeyEvent, KeyModifiers},
    execute,
    terminal::{EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    Frame, Terminal,
    backend::CrosstermBackend,
    layout::{Alignment, Rect},
    style::Stylize,
    text::Line,
    widgets::{Block, Paragraph, Wrap},
};

use crate::{App, edit};

pub struct EvergreenMode {
    pub line_offset: u16,
}

/// Handle user input in evergreen mode.
pub fn handle_input(
    app: &mut App,
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    key_event: KeyEvent,
) -> io::Result<()> {
    match key_event.code {
        KeyCode::Char('j') | KeyCode::Down => app.evergreenmode.line_offset += 1,
        KeyCode::Char('k') | KeyCode::Up => {
            if app.evergreenmode.line_offset > 0 {
                app.evergreenmode.line_offset -= 1;
            }
        }
        KeyCode::PageDown => app.evergreenmode.line_offset += 20,
        KeyCode::PageUp => {
            if app.evergreenmode.line_offset >= 20 {
                app.evergreenmode.line_offset -= 20;
            } else {
                app.evergreenmode.line_offset = 0;
            }
        }
        KeyCode::Char('d') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                app.evergreenmode.line_offset += 20;
            }
        }
        KeyCode::Char('u') => {
            if key_event.modifiers == KeyModifiers::CONTROL {
                if app.evergreenmode.line_offset >= 20 {
                    app.evergreenmode.line_offset -= 20;
                } else {
                    app.evergreenmode.line_offset = 0;
                }
            }
        }
        KeyCode::Enter => {
            // Because edit calls another terminal program,
            // leave and enter alternate screen around it to ensure that
            // the screen gets cleared properly.
            // (Without this, there are remnants of taskfinder's
            // interface left over after exiting if a file is opened
            // at any point during its use.)
            execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
            edit(app.config.evergreen_file.clone())?;
            execute!(terminal.backend_mut(), EnterAlternateScreen)?;
            // Force redraw of everything.
            terminal.clear()?;
            app.evergreenmode.line_offset = 0;
        }
        _ => (),
    }
    Ok(())
}

pub fn render(
    app: &mut App,
    frame: &mut Frame,
    mut info_block: Block,
    mut main_block: Block,
    top_right: Rect,
    left: Rect,
    controls_content: &mut Vec<Line<'_>>,
) {
    let text = match fs::read_to_string(&app.config.evergreen_file) {
        Ok(v) => v,
        Err(e) => format!("Error opening evergreen file: {e}"),
    };
    main_block = main_block
        .title(" Evergreen ".bold())
        .title_alignment(Alignment::Center);
    frame.render_widget(
        Paragraph::new(text)
            .wrap(Wrap { trim: false })
            .scroll((app.evergreenmode.line_offset, 0))
            .block(main_block),
        left,
    );
    info_block = info_block.title_top(Line::from(" Info ").centered());
    frame.render_widget(
                Paragraph::new(vec![Line::from(
                    "This mode allows you to have a particular file accessible at the touch of a keystroke.",
                )])
                .wrap(Wrap { trim: false })
                .block(info_block),
                top_right,
            );

    // Controls specific to this mode.
    controls_content.push(Line::from(""));
    controls_content.push(Line::from("Navigation".blue().bold().underlined()));
    controls_content.push(
        vec![
            "↑".blue(),
            " or ".into(),
            "k".blue(),
            " Scroll up 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "↓".blue(),
            " or ".into(),
            "j".blue(),
            " Scroll down 1 line".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgDn".blue(),
            " or ".into(),
            "CTRL+d".blue(),
            " Scroll down 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(
        vec![
            "PgUp".blue(),
            " or ".into(),
            "CTRL+u".blue(),
            " Scroll up 20 lines".into(),
        ]
        .into(),
    );
    controls_content.push(vec!["Enter".blue(), " Edit file".into()].into());
}
