#!/usr/bin/env fish

cargo fetch --locked

# Build for Linux
set LINUX x86_64-unknown-linux-gnu
cargo build --release --frozen --target $LINUX
mv target/$LINUX/release/tf target/$LINUX/release/tf-linux

# Build for asshole corporation OS
set WINDOWS x86_64-pc-windows-gnu
cargo build --release --frozen --target $WINDOWS
mv target/$WINDOWS/release/tf.exe target/$WINDOWS/release/tf-windows.exe

# Build for another asshole corporation OS
set MAC x86_64-apple-darwin
export SDKROOT=$(pwd)/MacOSX13.3.sdk/
export CARGO_TARGET_X86_64_APPLE_DARWIN_LINKER=rust-lld
if test ! -d MacOSX*
  curl -L https://github.com/roblabla/MacOSX-SDKs/releases/download/13.3/MacOSX13.3.sdk.tar.xz | tar xJ
end
rustup target add $MAC
cargo build --release --frozen --target $MAC
mv target/$MAC/release/tf target/$MAC/release/tf-mac
