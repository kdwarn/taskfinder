# Installation

The recommended way to install is by using Rust's cargo: `cargo install taskfinder`. You can then open it on the command line with `tf`.

You may also use pre-built binaries, which are available for Linux, Mac, and Windows at <https://codeberg.org/kdwarn/taskfinder/releases>. Download the version for your operating system and remove the OS name from the binary, e.g. rename tf-windows.exe to tf.exe.

Arch Linux users may install the [taskfinder](https://aur.archlinux.org/packages/taskfinder) package from the AUR [manually](https://wiki.archlinux.org/title/Arch_User_Repository#Installing_and_upgrading_packages) or using your favorite AUR helper.

NixOS users, with flakes enabled, can install taskfinder directly using `nix profile install git+https://codeberg.org/kdwarn/taskfinder`. Alternatively, if using home-manager with flakes, add taskfinder to the flake inputs and make the package available in `home.packages`:
```nix
{
  inputs = {
    # your other inputs...
    taskfinder.url = "git+https://codeberg.org/kdwarn/taskfinder";
  };

  config = { pkgs, ... }: {
    home.packages = [
      inputs.taskfinder.packages.${pkgs.system}.default
    ];
  };
}
```
